/**
 * WebTools-Platform
 * Copyright (C) 2016-2018  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.analytics.db;

/*-
 * #%L
 * webtools-analytics
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.alibaba.fastjson.JSONObject;
import org.testng.annotations.Test;
import com.thorstenmarx.webtools.api.analytics.Fields;
import com.thorstenmarx.webtools.base.Configuration;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import net.engio.mbassy.bus.MBassador;
import org.assertj.core.api.Assertions;
import org.awaitility.Awaitility;


/**
 *
 * @author thmarx
 */
public class ReopenAnalyticsDbTest {

	static DefaultAnalyticsDb instance;
	
	/**
	 * Test of open method, of class AnalyticsDb.
	 */
	@Test()
	public void testAnalyticsDb() throws Exception {

		System.out.println("running analytics db test");

		Configuration config = Configuration.empty();
		config.put("data", "dir", "target/adb-" + System.currentTimeMillis());

		MockedExecutor executor = new MockedExecutor();
		instance = new DefaultAnalyticsDb(config, new MBassador(), executor);

		instance.open();

		long timestamp = System.currentTimeMillis();
		JSONObject event = new JSONObject();

		
//		event.put(Shard.Field.TIMESTAMP.value(), ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
		event.put(Fields._TimeStamp.value(), timestamp);
		event.put("ua", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:51.0) Gecko/20100101 Firefox/51.0");
		event.put(Fields._UUID.value(), UUID.randomUUID().toString());
		

		JSONObject test = new JSONObject();
		test.put("name", "klaus");
		event.put("test", test);

		event.put("age", 25);
		event.put("average", 25.5);
		event.put("event", "pageLoaded");
		
		JSONObject meta = new JSONObject();
		meta.put("ip", "88.153.198.210");
		
		instance.track(TestHelper.event(event, new JSONObject()));
		
		
		Awaitility.waitAtMost(60, TimeUnit.SECONDS).until((Callable<Boolean>) () -> {
			System.out.println(instance.index().size());
			return instance.index().size() > 0;
		});
		
		instance.close();
		executor.shutdown();
		
		executor = new MockedExecutor();
		instance = new DefaultAnalyticsDb(config, new MBassador(), executor);

		instance.open();
		
		Thread.sleep(6000);
		
		Assertions.assertThat(instance.index().size()).isEqualTo(1);
		
		instance.close();
		executor.shutdown();
	}
	
}
