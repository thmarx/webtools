/**
 * WebTools-Platform
 * Copyright (C) 2016-2018  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.analytics.db;

/*-
 * #%L
 * webtools-analytics
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import static org.assertj.core.api.Assertions.*;
import org.testng.annotations.Test;

/**
 *
 * @author marx
 */
public class ExperimentDistributionTest {

	static final SecureRandom RANDOM = new SecureRandom();
	static final int MAX_PERCENT = 100;

	static final int TEST_COUNT = 10000;

	@Test
	public void experimentDistributionTest() {

		int experimentParticipationRate = 80;
		int countParticipate = 0;
		int countNotParticipate = 0;
		for (int i = 0; i < TEST_COUNT; i++) {
			boolean shouldParticipate = experimentParticipationRate >= RANDOM.nextDouble() * MAX_PERCENT;
			if (shouldParticipate) {
				countParticipate++;
			} else {
				countNotParticipate++;
			}
		}

		int participating = (countParticipate * 100) / TEST_COUNT;
		int notParticipating = (countNotParticipate * 100) / TEST_COUNT;

		assertThat(participating).isGreaterThan(75).isLessThan(82);
		assertThat(notParticipating).isGreaterThan(18).isLessThan(22);
	}

	public void testVariantDistribution_50_50() {
		List<Variant> variants = new ArrayList<>();
		variants.add(new Variant().name("v1").participationRate(50));
		variants.add(new Variant().name("v2").participationRate(50));

		MappedCounter counter = new MappedCounter();
		for (int i = 0; i < TEST_COUNT; i++){
			Variant selected = selectVariant(variants);
			counter.increment(selected.name());
		}
		
		assertThat((counter.getInt("v1") * 100 / 1000)).isGreaterThan(48).isLessThan(52);
		assertThat((counter.getInt("v2") * 100 / 1000)).isGreaterThan(48).isLessThan(52);
	}
	public void testVariantDistribution_80_20() {
		List<Variant> variants = new ArrayList<>();
		variants.add(new Variant().name("v1").participationRate(80));
		variants.add(new Variant().name("v2").participationRate(20));

		MappedCounter counter = new MappedCounter();
		for (int i = 0; i < TEST_COUNT; i++){
			Variant selected = selectVariant(variants);
			counter.increment(selected.name());
		}
		
		assertThat((counter.getInt("v1") * 100 / 1000)).isGreaterThan(78).isLessThan(82);
		assertThat((counter.getInt("v2") * 100 / 1000)).isGreaterThan(18).isLessThan(22);
	}
	public void testVariantDistribution_20_20_20() {
		List<Variant> variants = new ArrayList<>();
		variants.add(new Variant().name("v1").participationRate(80));
		variants.add(new Variant().name("v2").participationRate(20));

		MappedCounter counter = new MappedCounter();
		for (int i = 0; i < TEST_COUNT; i++){
			Variant selected = selectVariant(variants);
			counter.increment(selected.name());
		}
		
		assertThat((counter.getInt("v1") * 100 / 1000)).isGreaterThan(78).isLessThan(82);
		assertThat((counter.getInt("v2") * 100 / 1000)).isGreaterThan(18).isLessThan(22);
	}

	private Variant selectVariant(final List<Variant> variants) {
		Variant selectedVariant = null;

		int p = RANDOM.nextInt() * MAX_PERCENT;
		int cumulativeProbability = 0;
		for (Variant variant : variants) {
			cumulativeProbability += variant.participationRate();
			if (p <= cumulativeProbability) {
				selectedVariant = variant;
				break;
			}
		}
		/*
         for the case the distribution ist equal for all variantions, the above code will everytime select the first.
         so we have the chec if there are variants with the same ParticipationRate and select a random one 
		 */
		List<Variant> candidates = new ArrayList<>();
		for (final Variant variant : variants) {
			if (variant.participationRate() == selectedVariant.participationRate()) {
				candidates.add(variant);
			}
		}
		if (candidates.size() > 1) {
			selectedVariant = candidates.get(RANDOM.nextInt(candidates.size()));
		}
		return selectedVariant;
	}

	static class Variant {

		private String name;
		private int participationRate;

		public Variant name(final String name) {
			this.name = name;
			return this;
		}

		public Variant participationRate(final int participationRate) {
			this.participationRate = participationRate;
			return this;
		}

		public String name() {
			return name;
		}

		public int participationRate() {
			return participationRate;
		}
	}

	static class MappedCounter {

		private Map<String, AtomicInteger> map = new HashMap<>();

		public void increment(String k) {
			if (!map.containsKey(k)) {
				map.put(k, new AtomicInteger(0));
			}
			map.get(k).incrementAndGet();
		}

		public int getInt(String k) {
			return map.containsKey(k) ? map.get(k).intValue(): 0;
		}

		public Set<String> getKeys() {
			return map.keySet();
		}
	}
}
