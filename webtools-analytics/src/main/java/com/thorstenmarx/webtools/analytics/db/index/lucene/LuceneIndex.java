package com.thorstenmarx.webtools.analytics.db.index.lucene;

/*-
 * #%L
 * webtools-analytics
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.google.common.base.Charsets;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnel;
import com.google.common.hash.PrimitiveSink;
import com.thorstenmarx.webtools.analytics.db.index.IndexDocument;
import com.thorstenmarx.webtools.analytics.db.DefaultAnalyticsDb;
import com.thorstenmarx.webtools.analytics.db.index.Index;
import com.thorstenmarx.webtools.api.analytics.Fields;
import com.thorstenmarx.webtools.api.analytics.query.Query;

import com.thorstenmarx.webtools.api.analytics.query.ShardDocument;
import com.thorstenmarx.webtools.base.Configuration;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.store.AlreadyClosedException;

/**
 *
 * @author marx
 */
public class LuceneIndex implements Index, AutoCloseable {

	private static final Logger LOGGER = LogManager.getLogger(LuceneIndex.class);

	private final Configuration configuration;
	/**
	 * List of all shards, normaly only the newest is writable
	 */
	private final List<Shard> shards = new ArrayList<>();

	private static final int DEFAULT_SHARD_COUNT = 10;
	private int shardCount = DEFAULT_SHARD_COUNT;
	
	public static Map<String, Object> DEFAULT_CONFIG = new HashMap<>();
	static {
		DEFAULT_CONFIG.put("shards", DEFAULT_SHARD_COUNT);
	}
	final Map<String, Object> configMap;

	private String indexPath;

	private final Properties indexConfiguration = new Properties();

	final DefaultAnalyticsDb adb;

	private final ShardSelectionStrategy shardSelectionStrategy;

	private boolean closed = false;
	
	// shard for realtime queries
//	private MemoryShard memoryShard;

	/**
	 *
	 * @param configuration The Configuration.
	 * @param adb The database.
	 */
	public LuceneIndex(final Configuration configuration, final DefaultAnalyticsDb adb) {
		this.configuration = configuration;
		this.adb = adb;
//		this.shardSelectionStrategy = new HashShardSelectionStrategy();
		this.shardSelectionStrategy = new RandomShardSelectionStrategy();
		
		Configuration.Config<Map<String, Object>> config = configuration.getConfig("analytics", Map.class);
		
		
		configMap = config.get(DEFAULT_CONFIG);
		
		this.shardCount = (int) configMap.get("shards");
//		this.memoryShard = new MemoryShard();
	}

	/**
	 *
	 * @throws IOException If something goes wrong.
	 */
	@Override
	public Index open() throws IOException {
		indexPath = (String) configuration.getMap("data", Collections.EMPTY_MAP).get("dir");

		if (!indexPath.endsWith("/")) {
			indexPath += "/";
		}
		indexPath += DefaultAnalyticsDb.ANALYTICS_DIR + "/index/";

		File config = new File(indexPath, "index.properties");
		if (config.exists()) {
			indexConfiguration.load(new FileReader(config));
		}

		File indexDir = new File(indexPath);
		if (!indexDir.exists()) {
			indexDir.mkdirs();
		}

		// load shards
		String[] availableShards = indexDir.list((File current, String name) -> new File(current, name).isDirectory());
		Arrays.sort(availableShards);
		// open shards
		int count = 0;
		for (String name : availableShards) {
			Shard shard = new Shard(name, configuration, adb);
			shard.open();
			shards.add(count, shard);
			count++;
		}
		while (count < shardCount) {
			createShard();
			count++;
		}
		
		closed = false;
		
		return this;
	}

	private int getNextShardID() throws IOException {
		int index = Integer.parseInt(indexConfiguration.getProperty("shard.id", "0"));
		index++;

		Map<String, String> properties = new HashMap<>();
		properties.put("shard.id", String.valueOf(index));
		setPropertiesAndSave(properties);

		return index;
	}

	private void setPropertiesAndSave(final Map<String, String> properties) throws IOException {
		properties.entrySet().forEach((entry) -> {
			indexConfiguration.setProperty(entry.getKey(), entry.getValue());
		});

		File indexProperties = new File(indexPath + "index.properties");
		indexConfiguration.store(new FileWriter(indexProperties), "update properties");
	}

	/**
	 *
	 */
	public void reopen() {
		shards.forEach((s) -> {
			s.reopen();
		});
	}

	/**
	 *
	 * @throws IOException
	 */
	private void createShard() throws IOException {
		String name = String.format("shard_%010d", getNextShardID());
		new File(indexPath + name).mkdir();
		Shard s = new Shard(name, configuration, this.adb);
		s.open();
		shards.add(s);
	}

	/**
	 *
	 * @throws IOException If something goes wrong.
	 */
	@Override
	public void close() throws IOException {
		if (!closed) {
			shards.forEach((shard) -> {
				try {
					shard.close();
				} catch (IOException ex) {
					LOGGER.error("error closing shard " + shard.name, ex);
				} catch (AlreadyClosedException e) {
					LOGGER.warn("shard storae closed already", e);
				}
			});
		}
		closed = true;
	}

	/**
	 * A simple bulk import.
	 *
	 * @param document The Document to add.
	 * @throws IOException If something goes wrong.
	 */
	@Override
	public void add(final IndexDocument document) throws IOException {
		if (document == null) {
			return;
		}
//		memoryShard.add(document);
		final Shard shard = selectShard(document);
		shard.add(document);
		shard.reopen_internal();
	}

	private Shard selectShard(final IndexDocument document) {
		return shards.get(shardSelectionStrategy.nextIndex(document.json.getString(Fields._UUID.value()), shards.size()));
	}

	protected List<Shard> getShards() {
		return shards;
	}

	/**
	 * Execute search.
	 *
	 * @param query The query for the search.
	 * @return List of ShardDocument.
	 */
	@Override
	public List<ShardDocument> search(Query query) {

		BloomFilter<ShardDocument> bloom = BloomFilter.create((Funnel<ShardDocument>) (ShardDocument from, PrimitiveSink into) -> {
			final String uuid = from.document.getString(Fields._UUID.value());
			into.putString(uuid, Charsets.UTF_8);
		}, 500);
		List<ShardDocument> result = new java.util.concurrent.CopyOnWriteArrayList<>();
		shards.stream().filter((shard) -> (shard.hasData(query.start(), query.end()))).forEach((shard) -> {
			try {
				final List<ShardDocument> shardResult = shard.search(query);
				result.addAll(shardResult);
				shardResult.stream().parallel().forEach(bloom::put);
			} catch (IOException ex) {
				LOGGER.error("", ex);
			}
		});
		
		
//		memoryShard.query(query).stream().filter((ShardDocument t) -> !bloom.mightContain(t)).forEach(result::add);
//		CompletableFuture.runAsync(() -> {
//			result.forEach(sd -> {
//				final String uuid = sd.document.getString(Fields._UUID.value());
//				memoryShard.remove(uuid);
//			});
//	
//		});
				
		return Collections.unmodifiableList(result);
	}

	@Override
	public long size() {
		long size = 0;
		for (Shard shard : shards) {
			try {
				size += shard.size();
			} catch (IOException ex) {
				LOGGER.error("", ex);
			}
		}
		return size;
	}
}
