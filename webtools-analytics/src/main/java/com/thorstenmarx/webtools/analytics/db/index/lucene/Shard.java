package com.thorstenmarx.webtools.analytics.db.index.lucene;

/*-
 * #%L
 * webtools-analytics
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.webtools.analytics.db.DefaultAnalyticsDb;
import com.thorstenmarx.webtools.analytics.db.index.IndexDocument;
import com.thorstenmarx.webtools.api.analytics.Fields;
import com.thorstenmarx.webtools.api.analytics.Searchable;
import com.thorstenmarx.webtools.api.analytics.query.Query;
import com.thorstenmarx.webtools.api.analytics.query.ShardDocument;
import com.thorstenmarx.webtools.base.Configuration;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexUpgrader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.NumericDocValues;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.SearcherFactory;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.NRTCachingDirectory;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Shard is a single lucene index with a giffen start and end time.
 *
 * @author marx
 */
public class Shard implements Searchable, Comparable<Shard> {

	private static final Logger LOGGER = LoggerFactory.getLogger(Shard.class);

	private long timeFrom;
	private long timeTo;

	protected final String name;
	private final Configuration configuration;
	protected String uuid;

	protected Version luceneVersion = Version.LATEST;

	private File shardDir;

	private Directory directory;
	private IndexWriter writer = null;

	private SearcherManager nrt_manager;
	private NRTCachingDirectory nrt_index;

	private Future<?> reopenTask;

	final DefaultAnalyticsDb adb;

	private final Properties shardConfiguration = new Properties();

//	private TransLog translog;
	private final DocumentBuilder documentBuilder;

	final Lock commitLock = new ReentrantLock();

	private boolean closed = false;
	
	final Map<String, Object> dataConfig;

	public Shard(final String name, final Configuration configuration, final DefaultAnalyticsDb adb) {
		this.name = name;
		this.configuration = configuration;
		this.adb = adb;

		documentBuilder = new DocumentBuilder();
		
		Configuration.Config<Map<String, Object>> config = configuration.getConfig("data", Map.class);
		dataConfig = config.get(Collections.EMPTY_MAP);
	}

	private void update(final Directory directory) throws IOException {
		// check if update is needed
		if (!shardConfiguration.containsKey("shard.uuid")) {
			return;
		}
		String luceneVersionProperty = shardConfiguration.getProperty("lucene.version", null);
		if (luceneVersionProperty == null) {
			// thats the case if you update a old version withour index upgrade functionality
			LOGGER.debug("very old index format found, try to upgrade to latest version");
			updateToLatestIndexVersion(directory);
		} else {
			try {
				Version version = Version.parse(luceneVersionProperty);
				if (!Version.LATEST.equals(version)) {
					LOGGER.debug("you are using an old index format, try to update your index");
					updateToLatestIndexVersion(directory);
				} else {
					LOGGER.debug("your index is uptodate, not upgraded needed");
				}
			} catch (ParseException ex) {
				LOGGER.error("", ex);
				throw new IllegalStateException("could not parse index version", ex);
			}
		}
	}

	private void updateToLatestIndexVersion(final Directory directory1) throws IOException {
		LOGGER.debug("upgrade to latest index version");
		luceneVersion = Version.LATEST;
		IndexUpgrader upgrader = new IndexUpgrader(directory1);
		upgrader.upgrade();
		LOGGER.debug("upgrade done");
		saveConfiguration();
		LOGGER.debug("configuration saved");
	}

	public void open() throws IOException {
		String indexDir = (String) dataConfig.get("dir");
		if (!indexDir.endsWith("/")) {
			indexDir += "/";
		}
		indexDir += DefaultAnalyticsDb.ANALYTICS_DIR + "/index/";

		shardDir = new File(indexDir + name);
		directory = FSDirectory.open(Paths.get(shardDir.toURI()));

		File shardPropertyFile = new File(shardDir, "shard.properties");
		if (shardPropertyFile.exists()) {
			shardConfiguration.load(new FileReader(shardPropertyFile));
		}
		uuid = shardConfiguration.getProperty("shard.uuid", UUID.randomUUID().toString());
		
		update(directory);

		Analyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
		indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
		indexWriterConfig.setCommitOnClose(true);
		nrt_index = new NRTCachingDirectory(directory, 5.0, 60.0);
		writer = new IndexWriter(nrt_index, indexWriterConfig);

		final SearcherFactory sf = new SearcherFactory();
		nrt_manager = new SearcherManager(writer, true, true, sf);

		this.reopenTask = adb.getExecutor().scheduleFixedRate(new ReopenTask(), 1000l, 10l, TimeUnit.MILLISECONDS);

		this.timeTo = getMaxTimestamp();
		this.timeFrom = getMinTimestamp();

		saveConfiguration();
	}

	private void saveConfiguration() throws IOException {
		File shardPropertyFile = new File(shardDir, "shard.properties");
//		shardConfiguration.put("shard.maxtime", String.valueOf(getMaxTimestamp()));
//		shardConfiguration.put("shard.mintime", String.valueOf(getMinTimestamp()));
		shardConfiguration.put("shard.maxtime", String.valueOf(timeTo));
		shardConfiguration.put("shard.mintime", String.valueOf(timeFrom));
		shardConfiguration.put("shard.name", name);
		shardConfiguration.put("shard.uuid", uuid);
		shardConfiguration.put("lucene.version", luceneVersion.toString());
		shardConfiguration.store(new FileWriter(shardPropertyFile), "shard configuration");
	}

	public void close() throws IOException {
		if (closed) {
			return;
		}
		closed = true;
		saveConfiguration();

		if (reopenTask != null) {
			reopenTask.cancel(true);
		}

		if (writer != null) {

			commitLock.lock();
			try {
				writer.close();
			} finally {
				commitLock.unlock();
			}
			nrt_manager.close();

			writer = null;
		}

		if (directory != null) {
			directory.close();
		}

	}

	private void commit() throws IOException {
		commitLock.lock();
		try {
//			writer.prepareCommit();
			writer.commit();
		} finally {
			commitLock.unlock();
		}
	}

	protected void add_internal(final JSONObject json) throws IOException {
		final Document doc = documentBuilder.build(json);
		writer.addDocument(doc);

		long timestamp = doc.getField(Fields._TimeStamp.value()).numericValue().longValue();
		if (timestamp > timeTo || timeTo == -1) {
			timeTo = timestamp;
		}
		if (timestamp < timeFrom || timeFrom == -1) {
			timeFrom = timestamp;
		}
	}

	protected void add_internal(final String json) throws IOException {
		JSONObject jsonObj = JSONObject.parseObject(json);
		add_internal(jsonObj);
	}

	public void add(final IndexDocument indexDocument) throws IOException {
//		translog.add(indexDocument.json.toJSONString());
		add_internal(indexDocument.json);
	}

	public void delete(Term term) throws IOException {
		writer.deleteDocuments(term);
	}

	public void update(Term term, Document doc) throws IOException {
		writer.updateDocument(term, doc);
	}

	@Override
	public List<ShardDocument> search(final Query query) throws IOException {
		IndexSearcher indexSearcher = nrt_manager.acquire();
		try {
			return internal_search(query, indexSearcher);
		} finally {
			nrt_manager.release(indexSearcher);
		}
	}

	private List<ShardDocument> internal_search(final Query query, IndexSearcher indexSearcher) throws IOException {

		BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();

		//NumericRangeQuery<Long> rangeQuery = NumericRangeQuery.newLongRange("timestamp_sort", query.start(), query.end(), true, true);
		org.apache.lucene.search.Query rangeQuery = LongPoint.newRangeQuery(Fields.TIMESTAMP_SORT.value(), query.start(), query.end());
		queryBuilder.add(rangeQuery, BooleanClause.Occur.FILTER);

		if (query.terms() != null && !query.terms().isEmpty()) {
			query.terms().entrySet().forEach((e) -> {
				queryBuilder.add(new TermQuery(new Term(e.getKey(), e.getValue())), BooleanClause.Occur.FILTER);
			});
		}

		if (query.multivalueTerms() != null && !query.multivalueTerms().isEmpty()) {
			query.multivalueTerms().entrySet().stream().map(Shard::multivalueTermsToBooleanQuery).forEach(booleanQuery -> {
				queryBuilder.add(booleanQuery, BooleanClause.Occur.FILTER);
			});
		}

		DocumentCollector collector = new DocumentCollector(indexSearcher.getIndexReader().maxDoc());

		BooleanQuery booleanQuery = queryBuilder.build();

		indexSearcher.search(booleanQuery, collector);

		BitSet hits = collector.hits();
		final List<ShardDocument> result = new ArrayList<>();

		hits.stream().forEach((i) -> {
			try {
				Document doc = indexSearcher.doc(i);
				final String source = doc.get("_source");
				final JSONObject sourceJson = JSONObject.parseObject(source);
				result.add(new ShardDocument(name, sourceJson));
			} catch (IOException ex) {
				LOGGER.error("", ex);
			}
		});

		return Collections.unmodifiableList(result);
	}

	@Override
	public boolean hasData(long from, long to) {
		return (from >= timeFrom && from <= timeTo) // from liegt innerhalb des Shards
				|| (to >= timeFrom && to <= timeTo) // to liegt innerhalb des Shards
				|| (from <= timeFrom && to >= timeTo); // der komplette Shard ist eine Teilmenge
	}

	public long getTimeFrom() {
		return timeFrom;
	}

	public long getTimeTo() {
		return timeTo;
	}

	public int size() throws IOException {
		IndexSearcher localSearcher = nrt_manager.acquire();
		try {
			return localSearcher.getIndexReader().numDocs();
		} finally {
			nrt_manager.release(localSearcher);
		}
	}

	public long getMaxTimestamp() throws IOException {
		IndexSearcher localSearcher = nrt_manager.acquire();
		try {
			return getMaxTimestamp(localSearcher.getIndexReader());
		} finally {
			nrt_manager.release(localSearcher);
		}
	}

	private long getMaxTimestamp(IndexReader reader) throws IOException {
		long max = -1;
		try {
			for (LeafReaderContext ctx : reader.leaves()) {
				final NumericDocValues longs
						= ctx.reader().getNumericDocValues(Fields._TimeStamp.value());
				int docid = DocIdSetIterator.NO_MORE_DOCS;
				while ((docid = longs.nextDoc()) != DocIdSetIterator.NO_MORE_DOCS) {
					if (max == -1) {
						max = longs.longValue();
					} else {
						max = Math.max(max, longs.longValue());
					}
				}
//				for (int i = 0; i < ctx.reader().maxDoc(); ++i) {
//					if (max == -1) {
//						max = longs.get(i);
//					} else {
//						max = Math.max(max, longs.get(i));
//					}
//				}
			}
		} catch (Exception e) {
			LOGGER.error("", e);
		}

		return max;
	}

	private long getMinTimestamp() throws IOException {
		IndexSearcher localSearcher = nrt_manager.acquire();
		try {
			return getMinTimestamp(localSearcher.getIndexReader());
		} finally {
			nrt_manager.release(localSearcher);
		}
	}

	private long getMinTimestamp(IndexReader reader) throws IOException {
		long min = -1;
		try {
			for (LeafReaderContext ctx : reader.leaves()) {
				final NumericDocValues longs
						= ctx.reader().getNumericDocValues(Fields._TimeStamp.value());
				int docid = DocIdSetIterator.NO_MORE_DOCS;
				while ((docid = longs.nextDoc()) != DocIdSetIterator.NO_MORE_DOCS) {
					if (min == -1) {
						min = longs.longValue();
					} else {
						min = Math.min(min, longs.longValue());
					}
				}
//				for (int i = 0; i < ctx.reader().maxDoc(); ++i) {
//					if (longs == null) {
//						min = -1;
//					} else if (min == -1) {
//						min = longs.get(i);
//					} else {
//						min = Math.min(min, longs.get(i));
//					}
//				}
			}
		} catch (Exception e) {
			LOGGER.error("", e);
		}

		return min;
	}

	void reopen() {
		try {
			commit();
			nrt_manager.maybeRefreshBlocking();
		} catch (IOException ex) {
			LOGGER.error("", ex);
		}
	}
	void reopen_internal() {
		try {
			nrt_manager.maybeRefresh();
		} catch (IOException ex) {
			LOGGER.error("", ex);
		}
	}

	@Override
	public int compareTo(Shard t) {
		String num1 = name.substring(name.lastIndexOf("_") + 1);
		String num2 = t.name.substring(t.name.lastIndexOf("_") + 1);

		return num1.compareTo(num2);
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Shard other = (Shard) obj;
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		return true;
	}

	private class ReopenTask implements Runnable {

		public ReopenTask() {
		}

		@Override
		public void run() {
			LOGGER.debug("reopen db");
			if (writer.isOpen()) {
				reopen();
			}
		}
	}

	protected static BooleanQuery multivalueTermsToBooleanQuery(Map.Entry<String, String[]> entry) {
		BooleanQuery.Builder subQuery = new BooleanQuery.Builder();

		String key = entry.getKey();
		String values[] = entry.getValue();
		for (String value : values) {
			subQuery.add(new TermQuery(new Term(key, value)), BooleanClause.Occur.SHOULD);
		}
		return subQuery.build();
	}
}
