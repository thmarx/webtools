package com.thorstenmarx.webtools.api.datalayer;

/*-
 * #%L
 * webtools-api
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.thorstenmarx.webtools.stream.ImmutableCollectionCollectors;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author marx
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class SegmentData {
	
	public static final String KEY = "segments";

	@XmlElement
	@XmlElementWrapper(name = "segment")
	public Set<Segment> segments;

	public SegmentData() {
		segments = new HashSet<>();
	}

	/**
	 * Returns a copy of valid segments.
	 *
	 * @return
	 */
	public Set<String> getSegments() {
		final long current = System.currentTimeMillis();
		return segments.stream().filter((s) -> s.validTo >= current).map((s) -> s.name).collect(ImmutableCollectionCollectors.toImmutableSet());
	}

	public void addSegment(final String name, final long validTo) {
		segments.add(new Segment(name, validTo));
	}
	public void addSegment(final String name) {
		segments.add(new Segment(name, Long.MAX_VALUE));
	}

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.NONE)
	public static class Segment {

		@XmlAttribute
		public String name;
		@XmlAttribute
		public long validTo;

		public Segment() {
		}

		public Segment(final String name, final long validTo) {
			this.name = name;
			this.validTo = validTo;
		}
	}
}
