package com.thorstenmarx.webtools.api.entities;

/*-
 * #%L
 * webtools-api
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;

/**
 *
 * @author marx
 */
public class Result<T> extends ArrayList<T> {

	public static final Result EMPTY = new Result(0, 0, 0);
	private static final long serialVersionUID = 8605787757855134693L;

	private final int totalSize;
	private final int offset;
	private final int limit;

	public Result(final int totalSize, final int offset, final int limit) {
		super();
		this.totalSize = totalSize;
		this.offset = offset;
		this.limit = limit;
	}

	public int totalSize() {
		return totalSize;
	}

	public int offset() {
		return offset;
	}

	public int limit() {
		return limit;
	}

}
