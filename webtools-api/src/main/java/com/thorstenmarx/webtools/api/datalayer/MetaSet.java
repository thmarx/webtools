package com.thorstenmarx.webtools.api.datalayer;

/*-
 * #%L
 * webtools-api
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import java.util.Collections;
import java.util.Set;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author marx
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class MetaSet<T> {
	@XmlElement
	@XmlElementWrapper(name = "value")
	private Set<T> values;

	public MetaSet (){}
	
	public MetaSet (final Set<T> values) {
		this.values = values;
	}
	
	public Set<T> getValues() {
		if (values == null) {
			return Collections.EMPTY_SET;
		}
		return Collections.unmodifiableSet(values);
	}

	public void setValues(Set<T> values) {
		this.values = values;
	}
	
	
}
