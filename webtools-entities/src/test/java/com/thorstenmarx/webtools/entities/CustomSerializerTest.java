package com.thorstenmarx.webtools.entities;

/*-
 * #%L
 * webtools-entities
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.thorstenmarx.webtools.api.entities.Entities;
import com.thorstenmarx.webtools.api.entities.Result;
import com.thorstenmarx.webtools.api.entities.Serializer;
import com.thorstenmarx.webtools.api.entities.Store;
import com.thorstenmarx.webtools.api.entities.criteria.Restrictions;
import com.thorstenmarx.webtools.api.model.Pair;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author marx
 */
public class CustomSerializerTest {

	Entities entities;

	@BeforeMethod
	public void before() {
		entities = new EntitiesImpl("./build/db-" + System.nanoTime());
		((EntitiesImpl) entities).open();
	}

	@AfterMethod
	public void shutdown() throws Exception {
		((EntitiesImpl) entities).close();
	}

	@Test
	public void testSaveAndGet() {
		Store<Content> store = entities.store(Content.class, new Serializer<Content>() {
			@Override
			public Pair<String, String> serialize(Content object) {
				return new Pair("v1", object.getVorname());
			}

			@Override
			public Pair<String, Content> deserialize(String version, String content) {
				return new Pair<>("v1", new Content().setVorname(content));
			}
		});

		Content c = new Content().setVorname("Thorsten");
		final String id = store.save(c);

		Content c2 = store.get(id);
		Assertions.assertThat(c2).isNotNull();
		Assertions.assertThat(c2.getVorname()).isEqualTo("Thorsten");
	}

}
