package com.thorstenmarx.webtools.datalayer;

/*-
 * #%L
 * webtools-datalayer
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.api.datalayer.DataLayer;
import com.thorstenmarx.webtools.api.datalayer.MetaSet;
import com.thorstenmarx.webtools.api.datalayer.SegmentData;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;
import java.util.Optional;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import org.h2.jdbcx.JdbcConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author marx
 */
public class DefaultDataLayer implements DataLayer {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultDataLayer.class);

	final File parent;

	private JdbcConnectionPool pool;

	private final JAXBContext jaxbContext;

	public DefaultDataLayer(final File parent) throws JAXBException {
		this.parent = parent;
		jaxbContext = JAXBContext.newInstance(SegmentData.class, MetaSet.class);
	}

	public void open() {
		try {
			File folder = new File(parent, "datalayer/");
			if (!folder.exists()) {
				folder.mkdirs();
			}
			Class.forName("org.h2.Driver");
			pool = JdbcConnectionPool.create("jdbc:h2:" + new File(folder, "db").getAbsolutePath() + "", "sa", "sa");
			init();
		} catch (ClassNotFoundException ex) {
			LOGGER.error("", ex);
			throw new RuntimeException(ex);
		}
	}

	private void init() {
		try (Connection connection = pool.getConnection();
				Statement st = connection.createStatement()) {
			st.execute("CREATE TABLE IF NOT EXISTS entities (uid VARCHAR(255), key VARCHAR(255), value CLOB, lastmodified TIMESTAMP, PRIMARY KEY(uid, key))");

			connection.commit();
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public int size() {
		try (Connection connection = pool.getConnection()) {

			String statement = "SELECT count(uid) as entityCount FROM entities";

			try (PreparedStatement ps = connection.prepareStatement(statement);
					ResultSet rs = ps.executeQuery()) {

				if (rs.next()) {
					return rs.getInt("entityCount");
				}
			}
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		return 0;
	}

	@Override
	public <T> Optional<T> get(final String uid, final String key, Class<T> clazz) {
		try (Connection connection = pool.getConnection()) {

			String statement = "SELECT * FROM entities WHERE uid = ? AND key = ?";

			try (PreparedStatement ps = connection.prepareStatement(statement)) {
				ps.setString(1, uid);
				ps.setString(2, key);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {

						try (Reader reader = rs.getClob("value").getCharacterStream()) {
							return Optional.ofNullable((T) jaxbContext.createUnmarshaller().unmarshal(reader));
						}
					}
				}
			}
			return Optional.empty();
		} catch (SQLException | JAXBException | IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public boolean exists(final String uid, final String key) {
		try (Connection connection = pool.getConnection()) {

			String statement = "SELECT uid, key FROM entities WHERE uid = ? AND key = ?";

			try (PreparedStatement ps = connection.prepareStatement(statement)) {
				ps.setString(1, uid);
				ps.setString(2, key);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {
						return true;

					}
				}
			}
			return false;
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public boolean add(final String uid, final String key, final Object value) {

		try (Connection connection = pool.getConnection()) {

			String statement = "MERGE INTO entities (uid, key, value, lastmodified) VALUES(?, ?, ?, ?)";
			try (
					PreparedStatement ps = connection.prepareStatement(statement);
					StringWriter writer = new StringWriter();) {

				jaxbContext.createMarshaller().marshal(value, writer);

				ps.setString(1, uid);
				ps.setString(2, key);
				ps.setClob(3, new StringReader(writer.toString()));
				ps.setDate(4, new Date(System.currentTimeMillis()));

				ps.execute();
			}
			connection.commit();
		} catch (SQLException | JAXBException | IOException ex) {
			throw new RuntimeException(ex);

		}

		return false;
	}

	/**
	 * Delete an entity and all attributes;
	 *
	 * @param uid
	 * @param key
	 */
	public void remove(final String uid, final String key) {
		try (Connection connection = pool.getConnection();
				PreparedStatement st = connection.prepareStatement("DELETE FROM entities WHERE uid = ? AND key = ?")) {

			st.setString(1, uid);
			st.setString(2, key);
			st.execute();

			connection.commit();
		} catch (SQLException ex) {
			throw new RuntimeException(ex);

		}
	}

	/**
	 * Close the entities instance and shutdown the database connection.
	 */
	public void close() {
		try (
				Connection connection = pool.getConnection();
				Statement st = connection.createStatement()) {

			st.execute("shutdown compact");

		} catch (SQLException ex) {
			throw new RuntimeException(ex);

		}
	}

}
