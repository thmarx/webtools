/**
 * WebTools-Platform
 * Copyright (C) 2016-2018  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.datalayer;

/*-
 * #%L
 * webtools-datalayer
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.api.datalayer.SegmentData;
import java.io.File;
import javax.xml.bind.JAXBException;
import org.assertj.core.api.Assertions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author marx
 */
public class DefaultDataLayerTest {

	private static final String TYPE = "atype";
	private static final String OTHER_TYPE = "otype";
	private static final String EMPTY_TYPE = "etype";

	private static final String NAME = "test name";

	DefaultDataLayer entities;

	String id;

	@BeforeClass
	public void before() throws JAXBException {
		entities = new DefaultDataLayer(new File("./build/datalayer" + System.currentTimeMillis()));
		entities.open();
	}

	@AfterClass
	public void after() throws JAXBException {
		entities.close();
	}

	@Test
	public void testAdd() {

		SegmentData data = new SegmentData();
		data.addSegment("eins");
		data.addSegment("zwei");

		entities.add("testAdd", "segments", data);
		data = entities.get("testAdd", "segments", SegmentData.class).get();
		Assertions.assertThat(data).isNotNull();
		Assertions.assertThat(data.getSegments()).isNotNull().isNotEmpty().containsExactlyInAnyOrder("eins", "zwei");
	}

	@Test(expectedExceptions = UnsupportedOperationException.class)
	public void testUnmodifiable() {

		SegmentData data = new SegmentData();
		data.getSegments().add("eins");
	}

	@Test()
	public void testUpdate() {

		SegmentData data = new SegmentData();
		
		data.addSegment("eins");
		
		int size = entities.size();
		entities.add("testUpdate", "segments", data);
		data = entities.get("testUpdate", "segments", SegmentData.class).get();
		Assertions.assertThat(data).isNotNull();
		Assertions.assertThat(data.getSegments()).isNotNull().isNotEmpty().containsExactlyInAnyOrder("eins");
		Assertions.assertThat(entities.size()).isNotNull().isEqualTo(size + 1);

		data.addSegment("zwei");
		entities.add("testUpdate", "segments", data);
		data = entities.get("testUpdate", "segments", SegmentData.class).get();
		Assertions.assertThat(data).isNotNull();
		Assertions.assertThat(data.getSegments()).isNotNull().isNotEmpty().containsExactlyInAnyOrder("eins", "zwei");
		Assertions.assertThat(entities.size()).isNotNull().isEqualTo(size + 1);
	}

	@Test
	public void testExpire() {

		SegmentData data = new SegmentData();
		data.addSegment("eins", System.currentTimeMillis() - 1000);

		entities.add("testExpire", "segments", data);
		data = entities.get("testExpire", "segments", SegmentData.class).get();
		Assertions.assertThat(data).isNotNull();
		Assertions.assertThat(data.getSegments()).isNotNull().isEmpty();
	}

	@Test
	public void testMore() {

		SegmentData data = new SegmentData();
		data.addSegment("eins");
		data.addSegment("zwei");

		final long before = System.currentTimeMillis();
		for (int i = 0; i < 1000; i++) {
			entities.add("testMore" + i, "segments", data);
		}
		final long after = System.currentTimeMillis();

		System.out.println("took: " + (after - before) + " ms");

	}

	
	@Test()
	public void testUpdateMulitpleTimes() {

		SegmentData data = new SegmentData();
		
		data.addSegment("eins");
		
		int size = entities.size();
		entities.add("testUpdateMulitpleTimes", "segments", data);
		
		data.addSegment("zwei");
		for (int i = 0; i < 1000; i++) {
			entities.add("testUpdateMulitpleTimes", "segments", data);
		}
	}
	
}
