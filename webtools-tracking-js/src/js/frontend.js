/**
 * The user segments are in the global TMA_CONFIG Objekt
 * TMA_CONFIG.user_segments = ["seg1", "seg2"]
 * 
 * Elemente werden unter folgenden Bedingungen angezeigt.
 * 1. 
 */
(function (frontend, document) {
    var CLASS_HIDDEN = "tma_hide";
    var SELECTOR_HIDDEN = "." + CLASS_HIDDEN;
    update = function (selectedSegments) {
        document.querySelectorAll(SELECTOR_HIDDEN).forEach(function (element) {
            element.classList.remove(CLASS_HIDDEN);
        });


        var groups = collectGroups();


        groups.forEach(function (group) {
            var matches = [];
            document.querySelectorAll("[data-tma-group=" + group + "]").forEach(function (element) {
                if (element.dataset.tmaPersonalization !== "enabled") {
                    return;
                }
                if (!matchs(element, selectedSegments)) {
                    element.classList.add(CLASS_HIDDEN);
                } else {
                    matches.push(element);
                }
            });
            console.log(matches);
            // remove the default
            if (matches.length > 1) {
                matches.filter(function (item) {
                    return item.dataset.tmaDefault === "yes"
                }).forEach(function (item) {
                    item.classList.add(CLASS_HIDDEN);
                });
            }
        });

    };
    matchs = function ($element, selectedSegments) {
        if ($element.dataset.tmaDefault === "yes") {
            return true;
        } else if ($element.dataset.tmaMatching === "all") {
            var segments = $element.dataset.tmaSegments.split(",");
            var matching = true;
            segments.forEach(function (s) {
                if (!selectedSegments.includes(s)) {
                    matching = false;
                }
            });
            return matching;
        } else if ($element.dataset.tmaMatching === "single") {
            var segments = $element.dataset.tmaSegments.split(",");
            var matching = false;
            segments.forEach(function (s) {
                if (selectedSegments.includes(s)) {
                    matching = true;
                }
            });
            return matching;
        }
        return false;
    };
    collectGroups = function () {
        var groups = [];
        document.querySelectorAll("[data-tma-group]").forEach(function (element) {
            var group = element.getAttribute("data-tma-group").trim();
            if (!groups.includes(group) && group !== "") {
                groups.push(group);
            }
        });
        return groups;
    };

    frontend.update = update;
}(window.webtools.Frontend = window.webtools.Frontend || {}, document));