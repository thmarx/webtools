(function (tracking, document) {

    webtools.Context.isDNT = navigator.doNotTrack == "yes" || navigator.doNotTrack == "1"
        || navigator.msDoNotTrack == "1" || window.doNotTrack == "1";
    webtools.Context.DAY = 24 * 60 * 60 * 1000;
    webtools.Context.HOUR = 60 * 60 * 1000;
    webtools.Context.MINUTE = 60 * 1000;

    tracking.init = function (host, site, page) {
        webtools.Context.site = site;
        webtools.Context.page = page;
        webtools.Context.host = host;
        webtools.Context.uid = "";			// the userid
        webtools.Context.rid = "";			// the requestid
        webtools.Context.vid = "";			// the visitid
        webtools.Context.pixelImage = new Image();
    };
    tracking.page = function (page) {
        webtools.Context.page = page;
    };
    tracking.customParameters = function (customParameters) {
        webtools.Context.custom_parameter = customParameters;
    };
    tracking.setCookieDomain = function (domain) {
        webtools.Context.cookieDomain = domain;
    };
    tracking.optOut = function () {
        webtools.Context.setCookie('_tma_trackingcookie', "opt-out", 365 * TMA.DAY);
    };
    tracking.dnt = function () {
        return webtools.Context.isDNT || document.cookie.indexOf("_tma_trackingcookie=opt-out") !== -1;
    }

    createDefaultParameters = function () {
        webtools.Context.rid = getUniqueID("_tma_rid", 3 * webtools.MINUTE);
        webtools.Context.vid = getUniqueID("_tma_vid", 1 * webtools.HOUR);
        webtools.Context.uid = getUniqueID("_tma_uid", 365 * webtools.DAY);
        var currentDate = new Date();
        return "&site=" + webtools.Context.site
            + "&page=" + webtools.Context.page
            + "&uid=" + webtools.Context.uid
            + "&reqid=" + webtools.Context.rid
            + "&vid=" + webtools.Context.vid
            + "&referrer=" + escape(document.referrer)
            + "&offset=" + currentDate.getTimezoneOffset()
            + "&_t=" + currentDate.getTime();
    };

    createCustomParameters = function () {
        var customParameterString = "";
        //var name = arguments.length === 1 ? arguments[0] + "_" : "";

        if (webtools.Context.custom_parameter) {
            var customParameters = webtools.Context.custom_parameter;
            if (customParameters !== null && typeof customParameters === 'object') {
                for (var p in customParameters) {
                    if (customParameters.hasOwnProperty(p)) {
                        var value = customParameters[p]
                        if (Array.isArray(value)) {
                            for (var item in value) {
                                customParameterString += "&c_" + p + '=' + value[item];
                            }
                        } else {
                            customParameterString += "&c_" + p + '=' + customParameters[p];
                        }
                    }
                }
            }
        }
        return customParameterString;
    };
    getUniqueID = function (cookiename, expire) {
        var aid = webtools.Cookie.getCookie(cookiename);
        if (aid === null || aid === "") {
            aid = webtools.Tools.uuid();
        }
        // update cookie on every request
        webtools.Cookie.setCookie(cookiename, aid, expire);
        return aid;
    };

    tracking.track = function (event) {
        if (!tracking.dnt()) {
            // opt-out cookie is not set
            webtools.Context.pixelImage.src = webtools.Context.host + "/pixel?event=" + event + createDefaultParameters() + createCustomParameters();
        }
    };

    tracking.register = function () {
        // opt-out cookie is not set
        // incude tracking pixle
        // user id
        //webtools.Context.uid = getUniqueID("_tma_uid", 365 * webtools.Context.DAY);
        // visit id
        //webtools.Context.vid = getUniqueID("_tma_vid", 1 * webtools.Context.HOUR);
        // new requestid for every request
        //webtools.Context.rid = webtools.Tools.uuid();
        //webtools.Cookie.setCookie("_tma_rid", webtools.Context.rid, 3 * webtools.Context.MINUTE);

        tracking.track("pageview");
    };

    tracking.score = function (scores) {
        if (!tracking.dnt()) {
            var scoreParameters = "";
            for (var key in scores) {
                scoreParameters += "&score_" + key + "=" + scores[key];
            }
            // opt-out cookie is not set
            webtools.Context.pixelImage.src = webtools.Context.host + "/pixel?event=score" + scoreParameters + createDefaultParameters() + createCustomParameters();
        }
    };

}(window.webtools.Tracking = window.webtools.Tracking || {}, document));