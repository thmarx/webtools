(function (cookie, document) {
    cookie.setCookieDomain = function (domain) {
        webtools.Context.cookieDomain = domain;
    };
    cookie.setCookie = function (cname, cvalue, expire) {
        var d = new Date();
        d.setTime(d.getTime() + expire);
        var expires = "expires=" + d.toUTCString();
        var domain = "";
        if (webtools.Context.cookieDomain) {
            domain = ";domain=" + webtools.Context.cookieDomain;
        }
        document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/" + domain;
    };

    cookie.getCookie = function (cname) {
        if (document.cookie.length > 0) {
            var c_start = document.cookie.indexOf(cname + "=");
            if (c_start !== -1) {
                c_start = c_start + cname.length + 1;
                var c_end = document.cookie.indexOf(";", c_start);
                if (c_end === -1) {
                    c_end = document.cookie.length;
                }
                return unescape(document.cookie.substring(c_start, c_end));
            }
        }
        return "";
    };
}(window.webtools.Cookie = window.webtools.Cookie || {}, document));