var gulp = require('gulp');
var concat = require('gulp-concat');
var minify = require('gulp-minify');

gulp.task('webtools-wp-backend', function () {
	return gulp.src([
		'src/js/webtools.js',
		'src/js/tools.js',
		'src/js/domready.js',
		'src/js/highlight.js'
	])
		.pipe(concat('webtools-wp-backend.js'))
		.pipe(minify())
		.pipe(gulp.dest('build/js'));
});

gulp.task('webtools-tracking', function () {
	return gulp.src([
		'src/js/webtools.js',
		'src/js/cookie.js',
		'src/js/tools.js',
		'src/js/domready.js',
		'src/js/tracking.js',
	])
		.pipe(concat('webtools-tracking.js'))
		.pipe(minify())
		.pipe(gulp.dest('build/js'));
});

gulp.task('webtools-frontend', function () {
	return gulp.src([
		'src/js/webtools.js',
		'src/js/frontend.js',
	])
		.pipe(concat('webtools-frontend.js'))
		.pipe(minify())
		.pipe(gulp.dest('build/js'));
});

gulp.task('package', ['webtools-wp-backend', 'webtools-tracking', 'webtools-frontend']);