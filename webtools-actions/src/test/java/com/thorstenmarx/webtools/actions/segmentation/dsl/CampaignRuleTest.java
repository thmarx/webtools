package com.thorstenmarx.webtools.actions.segmentation.dsl;

/*-
 * #%L
 * webtools-actions
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.actions.segmentation.*;
import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.webtools.actions.ActionSystem;
import com.thorstenmarx.webtools.actions.TestHelper;
import com.thorstenmarx.webtools.analytics.db.DefaultAnalyticsDb;
import com.thorstenmarx.webtools.api.MetaKeys;
import com.thorstenmarx.webtools.api.TimeWindow;
import com.thorstenmarx.webtools.api.datalayer.MetaSet;
import com.thorstenmarx.webtools.api.datalayer.SegmentData;
import com.thorstenmarx.webtools.api.actions.SegmentService;
import com.thorstenmarx.webtools.api.actions.model.AdvancedSegment;
import com.thorstenmarx.webtools.api.analytics.Fields;
import com.thorstenmarx.webtools.base.Configuration;
import com.thorstenmarx.webtools.datalayer.DefaultDataLayer;
import com.thorstenmarx.webtools.tracking.referrer.ReferrerFilter;
import java.io.File;
import static org.assertj.core.api.Assertions.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import javax.xml.bind.JAXBException;
import net.engio.mbassy.bus.MBassador;
import org.awaitility.Awaitility;
import org.easymock.EasyMock;

/**
 *
 * @author thmarx
 */
public class CampaignRuleTest {

	DefaultAnalyticsDb analytics;
	ActionSystem actionSystem;
	SegmentService service;
	MockedExecutor executor;
	DefaultDataLayer datalayer;

	@BeforeClass
	public void setUpClass() throws JAXBException {
		long timestamp = System.currentTimeMillis();

		Configuration config = Configuration.empty();
		config.put("data", "dir", "target/adb-" + timestamp);

		MBassador mbassador = new MBassador();
		executor = new MockedExecutor();
		
		analytics = new DefaultAnalyticsDb(config, mbassador, executor);
		analytics.addFilter(ReferrerFilter::filter);
		analytics.open();
		

		service = new JAXBSegmentService("target/adb-" + timestamp);

		
		
		AdvancedSegment tester = new AdvancedSegment();
		tester.setId("twitter_test");
		tester.setName("Twitter Test");
		tester.setActive(true);
		tester.start(new TimeWindow(TimeWindow.UNIT.YEAR, 1));
		String sb = "segment().and(rule(CAMPAIGN).source('twitter').medium('tweet').campaign('test'))";
		tester.setDsl(sb);
		service.add(tester);
		
		tester = new AdvancedSegment();
		tester.setId("facebook_demo");
		tester.setName("FaceBook demo");
		tester.setActive(true);
		tester.start(new TimeWindow(TimeWindow.UNIT.YEAR, 1));
		sb = "segment().and(rule(CAMPAIGN).source('facebook').medium('post').campaign('demo'))";
		tester.setDsl(sb);
		service.add(tester);

		System.out.println("service: " + service.all());
		
		datalayer = new DefaultDataLayer(new File("target/dl-" + System.currentTimeMillis()));
		datalayer.open();
		
		actionSystem = new ActionSystem(analytics, service, config, null, mbassador, datalayer);
		actionSystem.start();
	}

	@AfterClass()
	public void tearDownClass() throws InterruptedException, Exception {
		actionSystem.close();
		analytics.close();
		datalayer.close();
	}

	@BeforeMethod
	public void setUp() {
	}

	@AfterMethod
	public void tearDown() {
	}

	/**
	 * Test of open method, of class AnalyticsDb.
	 *
	 * @throws java.lang.Exception
	 */
	@Test(invocationCount = 20)
	public void test_campaign_rule() throws Exception {

		System.out.println("test_campaign_rule");
		
		final String USER_ID = "user " + UUID.randomUUID().toString();

		JSONObject event = new JSONObject();
//		event.put("timestamp", ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
		event.put("_timestamp", System.currentTimeMillis());
		event.put("ua", "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:38.0) Gecko/20100101 Firefox/38.0");
		event.put("userid", USER_ID);
		event.put(Fields._UUID.value(), UUID.randomUUID().toString());
		event.put(Fields.VisitId.value(), UUID.randomUUID().toString());
		event.put("fingerprint", "fp_klaus");
		event.put("page", "testPage");
		event.put("site", "testSite");
		event.put("event", "pageview");
		event.put(Fields.Referrer.value(), "https://heise.de/?utm_source=twitter&utm_medium=tweet&utm_campaign=test");
		
		analytics.track(TestHelper.event(event, new JSONObject()));
		
		Optional<SegmentData> get = datalayer.get(USER_ID, SegmentData.KEY, SegmentData.class);
		
		Awaitility.await().atMost(60, TimeUnit.SECONDS).until(() -> datalayer.get(USER_ID, SegmentData.KEY, SegmentData.class).isPresent());
		Awaitility.await().atMost(60, TimeUnit.SECONDS).until(()
				-> datalayer.get(MetaKeys.META.value(), MetaKeys.SEGMENTED_USERS.value(), MetaSet.class).isPresent()
				&& !datalayer.get(MetaKeys.META.value(), MetaKeys.SEGMENTED_USERS.value(), MetaSet.class).get().getValues().isEmpty()
		);

		SegmentData data = datalayer.get(USER_ID, SegmentData.KEY, SegmentData.class).get();
		assertThat(data).isNotNull();

		MetaSet<String> segmented = datalayer.get(MetaKeys.META.value(), MetaKeys.SEGMENTED_USERS.value(), MetaSet.class).get();
		assertThat(segmented).isNotNull();
		assertThat(segmented.getValues()).isNotEmpty().contains(USER_ID);

		Set<String> segments = data.getSegments();

		assertThat(segments).isNotNull();
		assertThat(segments).containsExactly("twitter_test");
		
		event.put(Fields.VisitId.value(), UUID.randomUUID().toString());
		event.put(Fields.Referrer.value(), "https://heise.de/?utm_source=facebook&utm_medium=post&utm_campaign=demo");
		
		analytics.track(TestHelper.event(event, new JSONObject()));
						
		datalayer.remove(USER_ID, SegmentData.KEY);
		datalayer.remove(MetaKeys.META.value(), MetaKeys.SEGMENTED_USERS.value());

//		for (int i = 0; i < 10; i++) {
//			Thread.sleep(1000);
//			get = datalayer.get("klaus", SegmentData.KEY, SegmentData.class);
//			int a = 5;
//		}
		Awaitility.await().atMost(120, TimeUnit.SECONDS).until(() -> 
				datalayer.get(USER_ID, SegmentData.KEY, SegmentData.class).isPresent()
				&& datalayer.get(USER_ID, SegmentData.KEY, SegmentData.class).get().getSegments().size() == 2
		);
				
		data = datalayer.get(USER_ID, SegmentData.KEY, SegmentData.class).get();
		System.out.println("==== " + data.getSegments() + " ====");
		assertThat(data).isNotNull();
		segments = data.getSegments();

		assertThat(segments).isNotNull();
		assertThat(segments).containsExactlyInAnyOrder("twitter_test", "facebook_demo");
	}
}
