package com.thorstenmarx.webtools.actions.segmentation;

/*-
 * #%L
 * webtools-actions
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.webtools.actions.ActionSystem;
import com.thorstenmarx.webtools.actions.TestHelper;
import com.thorstenmarx.webtools.analytics.db.DefaultAnalyticsDb;
import java.util.HashMap;
import java.util.Map;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.thorstenmarx.webtools.api.TimeWindow;
import com.thorstenmarx.webtools.api.analytics.Fields;
import com.thorstenmarx.webtools.api.datalayer.SegmentData;
import com.thorstenmarx.webtools.api.actions.model.Segment;
import com.thorstenmarx.webtools.api.actions.model.rules.KeyValueRule;
import com.thorstenmarx.webtools.base.Configuration;
import com.thorstenmarx.webtools.datalayer.DefaultDataLayer;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.xml.bind.JAXBException;
import net.engio.mbassy.bus.MBassador;
import org.apache.lucene.store.AlreadyClosedException;
import org.assertj.core.api.Assertions;
import org.awaitility.Awaitility;

/**
 *
 * @author thmarx
 */
@Test(singleThreaded = true)
public class RulesTest {

	DefaultAnalyticsDb instance;
	ActionSystem actionSystem;
	MockedExecutor executor;
	
	DefaultDataLayer datalayer;

	@BeforeMethod
	public void setUp() throws IOException, JAXBException {
		Configuration config = Configuration.empty();
		
		config.put("data", "dir", "target/adb-" + System.nanoTime());

		MBassador mbassador = new MBassador();
		
		executor = new MockedExecutor();
		instance = new DefaultAnalyticsDb(config, mbassador, executor);
		instance.open();

		datalayer = new DefaultDataLayer(new File("target/dl-" + System.currentTimeMillis()));
		datalayer.open();
		
		actionSystem = new ActionSystem(instance, new JAXBSegmentService("target/segments + " + System.nanoTime()), config, null, mbassador, datalayer);
		actionSystem.start();
	}

	@AfterMethod
	public void tearDown() throws IOException, Exception {
		try {
			actionSystem.close();
			instance.close();
			datalayer.close();
			executor.shutdown();
		} catch (AlreadyClosedException ie) {
			// happens under test only
		}
	}

	/**
	 * Test of open method, of class AnalyticsDb.
	 *
	 * @throws java.lang.Exception
	 */
	@Test(enabled = false)
	public void test_keyvalue_rule() throws Exception {

		Segment iphoneUser = new Segment();
		iphoneUser.setId("iphone");
		iphoneUser.setName("iPhone");
		iphoneUser.start(new TimeWindow(TimeWindow.UNIT.YEAR, 1));
		KeyValueRule kv = new KeyValueRule();
		kv.key("device");
		kv.values(new String[]{"iphone"});
		iphoneUser.addRule(kv);
		actionSystem.addSegment(iphoneUser);

		Map<String, Object> event = new HashMap<>();
		event.put(Fields._TimeStamp.value(), System.currentTimeMillis());
		event.put("userid", "u1");
		event.put("device", "iphone");

		instance.track(TestHelper.event(event, new JSONObject()));

		Awaitility.await().atMost(60, TimeUnit.SECONDS).until(()
				-> datalayer.exists("u1", SegmentData.KEY) && !datalayer.get("u1", SegmentData.KEY, SegmentData.class).get().getSegments().isEmpty()
		);

		Set<String> segments = datalayer.get("u1", SegmentData.KEY, SegmentData.class).get().getSegments();
		Assertions.assertThat(segments).isNotNull().isNotEmpty().contains("iphone");
	}

}
