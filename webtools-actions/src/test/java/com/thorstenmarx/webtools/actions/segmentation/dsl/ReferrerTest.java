package com.thorstenmarx.webtools.actions.segmentation.dsl;

/*-
 * #%L
 * webtools-actions
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.actions.segmentation.*;
import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.webtools.actions.ActionSystem;
import com.thorstenmarx.webtools.actions.TestHelper;
import com.thorstenmarx.webtools.actions.dsl.DSLSegment;
import com.thorstenmarx.webtools.actions.dsl.rules.FirstVisitRule;
import com.thorstenmarx.webtools.actions.dsl.rules.ReferrerRule;
import com.thorstenmarx.webtools.analytics.db.DefaultAnalyticsDb;
import com.thorstenmarx.webtools.api.MetaKeys;
import com.thorstenmarx.webtools.api.TimeWindow;
import com.thorstenmarx.webtools.api.datalayer.MetaSet;
import com.thorstenmarx.webtools.api.datalayer.SegmentData;
import com.thorstenmarx.webtools.api.actions.SegmentService;
import com.thorstenmarx.webtools.api.actions.model.AdvancedSegment;
import com.thorstenmarx.webtools.api.analytics.Fields;
import com.thorstenmarx.webtools.api.analytics.query.ShardDocument;
import com.thorstenmarx.webtools.base.Configuration;
import com.thorstenmarx.webtools.datalayer.DefaultDataLayer;
import java.io.File;
import static org.assertj.core.api.Assertions.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import javax.xml.bind.JAXBException;
import net.engio.mbassy.bus.MBassador;
import org.awaitility.Awaitility;
import org.easymock.EasyMock;

/**
 *
 * @author thmarx
 */
public class ReferrerTest {

	DefaultAnalyticsDb analytics;
	ActionSystem actionSystem;
	SegmentService service;
	MockedExecutor executor;
	DefaultDataLayer datalayer;

	@BeforeClass
	public void setUpClass() throws JAXBException {
		long timestamp = System.currentTimeMillis();

		Configuration config = Configuration.empty();
		config.put("data", "dir", "target/adb-" + timestamp);

		MBassador mbassador = new MBassador();
		executor = new MockedExecutor();
		
		analytics = new DefaultAnalyticsDb(config, mbassador, executor);
		analytics.open();
		

		service = new JAXBSegmentService("target/adb-" + timestamp);

		
		
		AdvancedSegment tester = new AdvancedSegment();
		tester.setId("search");
		tester.setName("Search");
		tester.setActive(true);
		tester.start(new TimeWindow(TimeWindow.UNIT.YEAR, 1));
		String sb = "segment().and(rule(REFERRER).medium('SEARCH'))";
		tester.setDsl(sb);
		service.add(tester);
		
		tester = new AdvancedSegment();
		tester.setId("notsearch");
		tester.setName("Not Search");
		tester.setActive(true);
		tester.start(new TimeWindow(TimeWindow.UNIT.YEAR, 1));
		sb = "segment().not(rule(REFERRER).medium('SEARCH'))";
		tester.setDsl(sb);
		service.add(tester);

		System.out.println("service: " + service.all());
		
		datalayer = new DefaultDataLayer(new File("target/dl-" + System.currentTimeMillis()));
		datalayer.open();
		
		actionSystem = new ActionSystem(analytics, service, config, null, mbassador, datalayer);
		actionSystem.start();
	}

	@AfterClass
	public void tearDownClass() throws InterruptedException, Exception {
		actionSystem.close();
		analytics.close();
		datalayer.close();
	}

	@BeforeMethod
	public void setUp() {
	}

	@AfterMethod
	public void tearDown() {
	}

	/**
	 * Test of open method, of class AnalyticsDb.
	 *
	 * @throws java.lang.Exception
	 */
	@Test(invocationCount = 100)
	public void test_referrer_rule() throws Exception {

		System.out.println("testing referrer rule");
		
		final String USER_ID = "user" + UUID.randomUUID().toString();

		JSONObject event = new JSONObject();
		event.put(Fields.UserId.value(), USER_ID);
		event.put(Fields._TimeStamp.value(), System.currentTimeMillis());
		event.put(Fields.VisitId.value(), UUID.randomUUID().toString());
		event.put(Fields.Site.value(), "testSite");
		
		analytics.track(TestHelper.event(event, new JSONObject()));

		Awaitility.await().atMost(60, TimeUnit.SECONDS).until(() -> datalayer.get(USER_ID, SegmentData.KEY, SegmentData.class).isPresent());
		
		SegmentData data = datalayer.get(USER_ID, SegmentData.KEY, SegmentData.class).get();
		assertThat(data).isNotNull();


		Set<String> segments = data.getSegments();

		assertThat(segments).isNotNull();
		assertThat(segments).containsExactly("notsearch");
		
		event = new JSONObject();
		event.put(Fields.UserId.value(), USER_ID);
		event.put(Fields._TimeStamp.value(), System.currentTimeMillis());
		event.put(Fields.VisitId.value(), UUID.randomUUID().toString());
		event.put(Fields.Site.value(), "testSite");
		event.put(Fields.Referrer.combine("medium"), "SEARCH");
		event.put(Fields.Referrer.combine("source"), "Google");
		
		analytics.track(TestHelper.event(event, new JSONObject()));
						
		datalayer.remove(USER_ID, SegmentData.KEY);
		datalayer.remove(MetaKeys.META.value(), MetaKeys.SEGMENTED_USERS.value());
		
		Awaitility.await().atMost(60, TimeUnit.SECONDS).until(() -> datalayer.get(USER_ID, SegmentData.KEY, SegmentData.class).isPresent());

		
		data = datalayer.get(USER_ID, SegmentData.KEY, SegmentData.class).get();
		assertThat(data).isNotNull();
		segments = data.getSegments();

		assertThat(segments).isNotNull();
		assertThat(segments).containsExactly("search");
	}
	
	@Test(invocationCount = 100)
	public void simpleTest (){
		DSLSegment not_search = new DSLSegment();
		not_search.not(new ReferrerRule().medium("SEARCH"));
		DSLSegment search = new DSLSegment();
		search.and(new ReferrerRule().medium("SEARCH"));
		
		final String USER_ID = "user " + UUID.randomUUID().toString();
		
		JSONObject event = new JSONObject();
		event.put(Fields.UserId.value(), USER_ID);
		event.put(Fields.VisitId.value(), UUID.randomUUID().toString());
		event.put(Fields.Site.value(), "testSite");
		
		ShardDocument doc = new ShardDocument("s1", event);
		not_search.handle(doc);
		not_search.match();
		search.handle(doc);
		search.match();
		
		assertThat(not_search.matchs(USER_ID)).isTrue();
		assertThat(search.matchs(USER_ID)).isFalse();
		
		
		not_search = new DSLSegment();
		not_search.not(new ReferrerRule().medium("SEARCH"));
		search = new DSLSegment();
		search.and(new ReferrerRule().medium("SEARCH"));
		
		event = new JSONObject();
		event.put(Fields.UserId.value(), USER_ID);
		event.put(Fields.VisitId.value(), UUID.randomUUID().toString());
		event.put(Fields.Site.value(), "testSite");
		event.put(Fields.Referrer.combine("medium"), "SEARCH");
		event.put(Fields.Referrer.combine("source"), "Google");
		
		ShardDocument doc2 = new ShardDocument("s1", event);
		not_search.handle(doc);
		not_search.handle(doc2);
		not_search.match();
		search.handle(doc);
		search.handle(doc2);
		search.match();
		
		assertThat(not_search.matchs(USER_ID)).isFalse();
		assertThat(search.matchs(USER_ID)).isTrue();
	}
}
