package com.thorstenmarx.webtools.actions.segmentation.dsl;

/*-
 * #%L
 * webtools-actions
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.actions.segmentation.*;
import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.webtools.actions.ActionSystem;
import com.thorstenmarx.webtools.actions.TestHelper;
import com.thorstenmarx.webtools.analytics.db.DefaultAnalyticsDb;
import com.thorstenmarx.webtools.api.actions.ActionEvent;
import com.thorstenmarx.webtools.api.actions.ActionException;
import com.thorstenmarx.webtools.api.actions.SegmentService;
import com.thorstenmarx.webtools.api.analytics.Fields;
import com.thorstenmarx.webtools.base.Configuration;
import com.thorstenmarx.webtools.datalayer.DefaultDataLayer;
import java.io.File;
import java.util.UUID;
import javax.xml.bind.JAXBException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import net.engio.mbassy.bus.MBassador;
import org.easymock.EasyMock;

/**
 *
 * @author thmarx
 */
public class ActionTest {

	DefaultAnalyticsDb analytics;
	DefaultDataLayer datalayer;
	ActionSystem actionSystem;
	SegmentService service;
	private MBassador mbassador;
	MockedExecutor executor;

	@BeforeClass
	public void setUpClass() throws ActionException, JAXBException {
		
		mbassador = EasyMock.createNiceMock(MBassador.class);
		
		long timestamp = System.currentTimeMillis();

		Configuration config = Configuration.empty();
		config.put("data", "dir", "target/adb-" + timestamp);

		executor = new MockedExecutor();
		analytics = new DefaultAnalyticsDb(config, mbassador, executor);
		analytics.open();

		service = new JAXBSegmentService("target/adb-" + timestamp);

		datalayer = new DefaultDataLayer(new File("target/dl-" + System.currentTimeMillis()));
		datalayer.open();

		
		System.out.println("service: " + service.all());

		actionSystem = new ActionSystem(analytics, service, config, null, mbassador, datalayer);
//		actionSystem.start();
		
		String sb = "eventAction('testEvent').window(years(1)).and(rule(EVENT).site('testSite').event('order').count(2))";
		actionSystem.addAction("a1", sb);
	}

	@AfterClass
	public void tearDownClass() throws InterruptedException, Exception {
		actionSystem.close();
		analytics.close();
		datalayer.close();
		executor.shutdown();
	}

	@BeforeMethod
	public void setUp() {
	}

	@AfterMethod
	public void tearDown() {
	}

	/**
	 * Test of open method, of class AnalyticsDb.
	 *
	 * @throws java.lang.Exception
	 */
	@Test
	public void test_action() throws Exception {

		System.out.println("testing event rule");
		
		ActionEvent actionEvent = ActionEvent.builder().setUserid("peter2").setEvent("testEvent").build();
		EasyMock.expect(mbassador.publishAsync(actionEvent)).andAnswer(() -> null).atLeastOnce();
		mbassador.subscribe(actionSystem);
		EasyMock.expectLastCall().anyTimes();
		
		EasyMock.replay(mbassador);

		actionSystem.start();
		
		// test event
		JSONObject event = new JSONObject();
//		event.put("timestamp", ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
		event.put("_timestamp", System.currentTimeMillis());
		event.put("ua", "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:38.0) Gecko/20100101 Firefox/38.0");
		event.put("userid", "peter2");
		event.put(Fields._UUID.value(), UUID.randomUUID().toString());
		event.put("site", "testSite");
		event.put("fingerprint", "fp_peter2");
		event.put("event", "order");
		
		analytics.track(TestHelper.event(event, new JSONObject()));

		Thread.sleep(2000);

		analytics.track(TestHelper.event(event, new JSONObject()));

		Thread.sleep(5000l);

		EasyMock.verify(mbassador);
	}
}
