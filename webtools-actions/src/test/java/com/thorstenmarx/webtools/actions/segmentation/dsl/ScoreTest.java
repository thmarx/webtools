package com.thorstenmarx.webtools.actions.segmentation.dsl;

/*-
 * #%L
 * webtools-actions
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.actions.segmentation.*;
import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.webtools.actions.ActionSystem;
import com.thorstenmarx.webtools.actions.TestHelper;
import com.thorstenmarx.webtools.analytics.db.DefaultAnalyticsDb;
import com.thorstenmarx.webtools.api.MetaKeys;
import com.thorstenmarx.webtools.api.TimeWindow;
import com.thorstenmarx.webtools.api.datalayer.MetaSet;
import com.thorstenmarx.webtools.api.datalayer.SegmentData;
import com.thorstenmarx.webtools.api.actions.SegmentService;
import com.thorstenmarx.webtools.api.actions.model.AdvancedSegment;
import com.thorstenmarx.webtools.api.analytics.Fields;
import com.thorstenmarx.webtools.base.Configuration;
import com.thorstenmarx.webtools.datalayer.DefaultDataLayer;
import java.io.File;
import static org.assertj.core.api.Assertions.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import javax.xml.bind.JAXBException;
import net.engio.mbassy.bus.MBassador;
import org.awaitility.Awaitility;
import org.easymock.EasyMock;

/**
 *
 * @author thmarx
 */
public class ScoreTest {

	DefaultAnalyticsDb analytics;
	ActionSystem actionSystem;
	SegmentService service;
	MockedExecutor executor;
	DefaultDataLayer datalayer;

	@BeforeClass
	public void setUpClass() throws JAXBException {
		long timestamp = System.currentTimeMillis();

		Configuration config = Configuration.empty();
		config.put("data", "dir", "target/adb-" + timestamp);

		MBassador mbassador = new MBassador();
		executor = new MockedExecutor();
		
		analytics = new DefaultAnalyticsDb(config, mbassador, executor);
		analytics.open();
		

		service = new JAXBSegmentService("target/adb-" + timestamp);

		
		
		

		AdvancedSegment tester = new AdvancedSegment();
		tester.setId("demoSeg");
		tester.setName("DEMO");
		tester.setActive(true);
		tester.start(new TimeWindow(TimeWindow.UNIT.YEAR, 1));
		String sb = "segment().and(rule(SCORE).name('demo').score(100))";
		tester.setDsl(sb);
		service.add(tester);

		System.out.println("service: " + service.all());
		
		datalayer = new DefaultDataLayer(new File("target/dl-" + System.currentTimeMillis()));
		datalayer.open();
		
		actionSystem = new ActionSystem(analytics, service, config, null, mbassador, datalayer);
		actionSystem.start();
	}

	@AfterClass
	public void tearDownClass() throws InterruptedException, Exception {
		actionSystem.close();
		analytics.close();
		datalayer.close();
	}

	@BeforeMethod
	public void setUp() {
	}

	@AfterMethod
	public void tearDown() {
	}

	

	@Test
	public void test_score_rule() throws Exception {

		System.out.println("testing score rules");

		JSONObject event = new JSONObject();
//		event.put("timestamp", ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
		event.put("_timestamp", System.currentTimeMillis());
		event.put("ua", "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:38.0) Gecko/20100101 Firefox/38.0");
		event.put("userid", "peter");
		event.put(Fields._UUID.value(), UUID.randomUUID().toString());
		event.put("fingerprint", "fp_peter");
		event.put("score", Arrays.asList(new String[]{"demo:50"}));
		
		analytics.track(TestHelper.event(event, new JSONObject()));

//		Thread.sleep(5000);

		event = new JSONObject();
//		event.put("timestamp", ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
		event.put("_timestamp", System.currentTimeMillis());
		event.put("ua", "Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:38.0) Gecko/20100101 Firefox/38.0");
		event.put("userid", "peter");
		event.put(Fields._UUID.value(), UUID.randomUUID().toString());
		event.put("fingerprint", "fp_peer");
		event.put("score", Arrays.asList(new String[]{"demo:100"}));
		
		analytics.track(TestHelper.event(event, new JSONObject()));

		Awaitility.await().atMost(30, TimeUnit.SECONDS).until(() -> 
				datalayer.get("peter", SegmentData.KEY, SegmentData.class).isPresent()
		);

		Set<String> segments = datalayer.get("peter", SegmentData.KEY, SegmentData.class).get().getSegments();
		assertThat(segments).isNotNull();
		assertThat(segments).contains("demoSeg");

	}

	
}
