package com.thorstenmarx.webtools.actions;

/*-
 * #%L
 * webtools-actions
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.testng.annotations.Test;
import static com.thorstenmarx.webtools.actions.JsonSegmentTranslator.*;
import com.thorstenmarx.webtools.actions.dsl.DSL;
import com.thorstenmarx.webtools.actions.dsl.DSLSegment;
import javax.script.ScriptException;
import org.assertj.core.api.Assertions;

/**
 *
 * @author thmarx
 */
public class JsonSegmentTranslatorTest {
	
	

	@Test
	public void testSomeMethod() throws ScriptException {
		JSONObject segment = new JSONObject();
		segment.put(Fields.TYPE, Types.GROUP);
		segment.put(Fields.OPERATOR, Operators.AND);
		
		JSONArray rules = new JSONArray();
		JSONObject pageView = new JSONObject();
		pageView.put(Fields.TYPE, Types.PAGEVIEW);
		pageView.put(Fields.SITE, "asite");
		pageView.put(Fields.PAGE, "apage");
		pageView.put(Fields.COUNT, 1);
		rules.add(pageView);
		
		segment.put(Fields.RULES, rules);
		
		final String dsl = new JsonSegmentTranslator().translate(segment.toJSONString());
		
		System.out.println(dsl);
		
		final DSL dslParser = new DSL(null, null);
		DSLSegment segmentObj = dslParser.build(dsl);
		
		Assertions.assertThat(segmentObj).isNotNull();
	}
	@Test
	public void testFirstVisit() throws ScriptException {
		JSONObject segment = new JSONObject();
		segment.put(Fields.TYPE, Types.GROUP);
		segment.put(Fields.OPERATOR, Operators.AND);
		
		JSONArray rules = new JSONArray();
		JSONObject pageView = new JSONObject();
		pageView.put(Fields.TYPE, Types.FIRSTVISIT);
		pageView.put(Fields.SITE, "asite");
		rules.add(pageView);
		
		segment.put(Fields.RULES, rules);
		
		final String dsl = new JsonSegmentTranslator().translate(segment.toJSONString());
		
		System.out.println(dsl);
		
		final DSL dslParser = new DSL(null, null);
		DSLSegment segmentObj = dslParser.build(dsl);
		
		Assertions.assertThat(segmentObj).isNotNull();
	}
	
}
