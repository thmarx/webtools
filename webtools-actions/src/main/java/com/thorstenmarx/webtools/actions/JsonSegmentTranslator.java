package com.thorstenmarx.webtools.actions;

/*-
 * #%L
 * webtools-actions
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.webtools.tracking.referrer.Medium;
import java.util.stream.Collectors;

/**
 * Translator to translate to sjon of the segment editor into to script based
 * dsl.
 *
 * @author thmarx
 */
public class JsonSegmentTranslator {

	static final class Fields {

		private Fields () {}
		
		public static final String TYPE = "type";
		public static final String OPERATOR = "operator";
		public static final String RULES = "rules";
		public static final String VALUE = "value";
		public static final String PAGE = "page";
		public static final String SITE = "site";
		public static final String CAMPAIGN = "campaign";
		public static final String MEDIUM = "medium";
		public static final String SOURCE = "source";
		public static final String COUNT = "count";
		public static final String EVENT = "event";
		public static final String SCORE = "score";
		public static final String NAME = "name";
		public static final String COUNTRY = "country";
	}

	static final class Operators {
		private Operators () {}

		public static final String AND = "AND";
		public static final String OR = "OR";
		public static final String NOT = "NOT";
	}

	static final class Types {

		private Types () {}
		
		public static final String GROUP = "group";
		public static final String PAGEVIEW = "pageview";
		public static final String FIRSTVISIT = "firstvisit";
		public static final String DEVICE = "device";
		public static final String OS = "os";
		public static final String BROWSER = "browser";
		public static final String LOCATION = "location";
		public static final String EVENT = "event";
		public static final String SCORE = "score";
		public static final String CAMPAIGN = "campaign";
		public static final String REFERRER = "referrer";
	}

	/**
	 * erzeugt die dsl.
	 *
	 * segment().not()
	 *
	 * @param json
	 * @return
	 */
	public String translate(final String json) {
		StringBuilder dsl = new StringBuilder();

		JSONObject jsonObj = JSONObject.parseObject(json);

		dsl.append("segment().");
		dsl.append(handleGroup(jsonObj));
		dsl.append(";");

		return dsl.toString();
	}

	public String handleGroup(final JSONObject group) {
		StringBuilder sb = new StringBuilder();

		final String operator = group.getString(Fields.OPERATOR);
		if (null != operator) {
			switch (operator) {
				case Operators.AND:
					sb.append("and(");
					break;
				case Operators.OR:
					sb.append("or(");
					break;
				case Operators.NOT:
					sb.append("not(");
					break;
				default:
					break;
			}
		}

		if (group.containsKey(Fields.RULES)) {
			JSONArray rules = group.getJSONArray(Fields.RULES);
			String rulesString = rules.stream().map(o -> (JSONObject) o).map(this::handleRule).collect(Collectors.joining(", "));
			sb.append(rulesString);
		}

		sb.append(")");

		return sb.toString();
	}
	
	public String handleRule (final JSONObject rule) {
		StringBuilder sb = new StringBuilder();
		
		final String type = rule.getString(Fields.TYPE);
		
		if (null != type) {
			switch (type) {
				case Types.BROWSER:
					sb.append(handleBrowser(rule));
					break;
				case Types.DEVICE:
					sb.append(handleDevice(rule));
					break;
				case Types.EVENT:
					sb.append(handleEvent(rule));
					break;
				case Types.LOCATION:
					sb.append(handleLocation(rule));
					break;
				case Types.OS:
					sb.append(handleOS(rule));
					break;
				case Types.PAGEVIEW:
					sb.append(handlePageView(rule));
					break;
				case Types.FIRSTVISIT:
					sb.append(handleFirstVisit(rule));
					break;
				case Types.SCORE:
					sb.append(handleScore(rule));
					break;
				case Types.GROUP:
					sb.append(handleGroup(rule));
					break;
				case Types.CAMPAIGN:
					sb.append(handleCampaign(rule));
					break;
				case Types.REFERRER:
					sb.append(handleReferrer(rule));
					break;
				default: 
					break;
			}
		}
		
		return sb.toString();
	}
	
	public String handleReferrer (final JSONObject rule) {
		StringBuilder sb = new StringBuilder();
		
		
		sb.append("rule(REFERRER)");
		sb.append(".medium('").append(Medium.fromString(rule.getString(Fields.MEDIUM))).append("')");
		if (rule.containsKey(Fields.SOURCE)) { 
			sb.append(".source(").append(rule.getString(Fields.SCORE)).append(")");
		}
		
		return sb.toString();
	}
	
	public String handleCampaign (final JSONObject rule) {
		StringBuilder sb = new StringBuilder();
		
		
		sb.append("rule(SCORE)");
		sb.append(".campaign('").append(rule.getString(Fields.CAMPAIGN)).append("')");
		if (rule.containsKey(Fields.SOURCE)) { 
			sb.append(".source(").append(rule.getString(Fields.SCORE)).append(")");
		}
		if (rule.containsKey(Fields.MEDIUM)) { 
			sb.append(".medium(").append(rule.getString(Fields.MEDIUM)).append(")");
		}
		
		return sb.toString();
	}
	
	public String handleScore (final JSONObject rule) {
		StringBuilder sb = new StringBuilder();
		
		
		sb.append("rule(SCORE)");
		sb.append(".name('").append(rule.getString(Fields.NAME)).append("')");
		sb.append(".score(").append(rule.getString(Fields.SCORE)).append(")");
		
		return sb.toString();
	}
	public String handlePageView (final JSONObject rule) {
		StringBuilder sb = new StringBuilder();
		
		
		sb.append("rule(PAGEVIEW).site('").append(rule.getString(Fields.SITE)).append("')");
		if (rule.containsKey(Fields.PAGE)) {
			sb.append(".page('").append(rule.getString(Fields.PAGE)).append("')");
		}
		sb.append(".count(").append(rule.getString(Fields.COUNT)).append(")");
		
		return sb.toString();
	}
	public String handleFirstVisit (final JSONObject rule) {
		StringBuilder sb = new StringBuilder();
		
		
		sb.append("rule(FIRSTVISIT).site('").append(rule.getString(Fields.SITE)).append("')");
		
		return sb.toString();
	}
	
	public String handleEvent (final JSONObject rule) {
		StringBuilder sb = new StringBuilder();
		
		
		sb.append("rule(EVENT).site('").append(rule.getString(Fields.SITE)).append("')");
		sb.append(".event('").append(rule.getString(Fields.EVENT)).append("')");
		sb.append(".count(").append(rule.getString(Fields.COUNT)).append(")");
		
		return sb.toString();
	}
	
	public String handleBrowser (final JSONObject rule) {
		StringBuilder sb = new StringBuilder();
		
		
		sb.append("rule(KEYVALUE).key('browser.group').values(['").append(rule.getString(Fields.VALUE)).append("'])");
		
		return sb.toString();
	}
	
	public String handleDevice (final JSONObject rule) {
		StringBuilder sb = new StringBuilder();
		
		
		sb.append("rule(KEYVALUE).key('os.device').values(['").append(rule.getString(Fields.VALUE)).append("'])");
		
		return sb.toString();
	}
	public String handleOS (final JSONObject rule) {
		StringBuilder sb = new StringBuilder();
		
		
		sb.append("rule(KEYVALUE).key('os.group').values(['").append(rule.getString(Fields.VALUE)).append("'])");
		
		return sb.toString();
	}
	public String handleLocation (final JSONObject rule) {
		StringBuilder sb = new StringBuilder();
		
		
		sb.append("rule(KEYVALUE).key('location.country.iso').values(['").append(rule.getString(Fields.COUNTRY)).append("'])");
		
		return sb.toString();
	}

}
