package com.thorstenmarx.webtools.actions.segmentation;

/*-
 * #%L
 * webtools-actions
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.api.Clone;
import com.thorstenmarx.webtools.api.actions.SegmentService;
import com.thorstenmarx.webtools.api.actions.model.AdvancedSegment;
import com.thorstenmarx.webtools.api.actions.model.Segment;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author marx
 */
public class JAXBSegmentService implements SegmentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(JAXBSegmentService.class);

	public static final String FILENAME = "segments.xml";
	String path;

	SegmentContainer segmentContainer = new SegmentContainer();

	private final Map<String, Segment> segmentMap = new ConcurrentHashMap<>();

	private final List<ChangedEventListener> listeners = new CopyOnWriteArrayList<>();

	protected JAXBSegmentService() {
	}

	public JAXBSegmentService(final String path) {
		this.path = path;
		if (!path.endsWith("/")) {
			this.path += "/";
		}

		loadSegments();
	}

	@Override
	public synchronized void addEventListener(final ChangedEventListener listener) {
		listeners.add(listener);
	}

	@Override
	public synchronized void removeEventListener(final ChangedEventListener listener) {
		listeners.remove(listener);
	}

	@Override
	public void add(Segment segment) {
		this.segmentMap.put(segment.getId(), segment);
		this.segmentContainer.addAll(this.segmentMap.values());
		saveSegments();
		fireEvent(new ChangedEvent(this, ChangedEvent.Type.Update, segment));
	}

	@Override
	public void remove(String id) {
		Segment segment = this.segmentMap.remove(id);
		this.segmentContainer.addAll(this.segmentMap.values());
		saveSegments();
		fireEvent(new ChangedEvent(this, ChangedEvent.Type.Delete, segment));
	}

	@Override
	public Segment get(String id) {
		Segment segment = segmentMap.get(id);
		return Clone.clone(segment);
	}

	/**
	 * returns a unmodifiable collection.
	 *
	 * @return
	 */
	@Override
	public Collection<Segment> all() {
		return Collections.unmodifiableCollection(Clone.clone(segmentMap.values()));
	}

	private void loadSegments() {
		try {
			File file = new File(path, FILENAME);

			if (!file.exists()) {
				return;
			}

			JAXBContext jaxbContext = JAXBContext.newInstance(SegmentContainer.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			this.segmentContainer = (SegmentContainer) jaxbUnmarshaller.unmarshal(file);

			this.segmentContainer.segments.stream().forEach((s) -> {
				segmentMap.put(s.getId(), s);
			});
		} catch (JAXBException ex) {
			LOGGER.error("", ex);
			throw new RuntimeException(ex);
		}

	}

	private synchronized void saveSegments() {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(SegmentContainer.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(this.segmentContainer, new File(path, FILENAME));
		} catch (JAXBException ex) {
			LOGGER.error("", ex);
		}
	}

	private synchronized void fireEvent(ChangedEvent event) {
		listeners.forEach((eh) -> {
			eh.changed(event);
		});
	}

	@XmlRootElement(name = "segments")
	@XmlAccessorType(XmlAccessType.NONE)
	public static class SegmentContainer {

		@XmlElement(name = "segment")
		List<Segment> segments = new ArrayList<>();

		public List<Segment> getSegments() {
			return segments;
		}

		public void addAll(Collection<Segment> values) {
			this.segments.clear();
			this.segments.addAll(values);
		}

		public void setDescriptions(List<Segment> segments) {
			this.segments = segments;
		}
	}



}
