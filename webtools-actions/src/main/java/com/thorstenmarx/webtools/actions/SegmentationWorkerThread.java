package com.thorstenmarx.webtools.actions;

/*-
 * #%L
 * webtools-actions
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.google.common.collect.Sets;
import com.thorstenmarx.modules.api.ModuleManager;
import com.thorstenmarx.webtools.actions.dsl.AdvancedSegmentationQuery;
import com.thorstenmarx.webtools.actions.dsl.DSL;
import com.thorstenmarx.webtools.actions.dsl.DSLSegment;
import com.thorstenmarx.webtools.api.MetaKeys;
import com.thorstenmarx.webtools.api.datalayer.MetaSet;
import com.thorstenmarx.webtools.api.datalayer.SegmentData;
import com.thorstenmarx.webtools.api.actions.model.AdvancedSegment;
import com.thorstenmarx.webtools.api.actions.model.Segment;
import com.thorstenmarx.webtools.api.analytics.AnalyticsDB;
import com.thorstenmarx.webtools.api.analytics.query.Aggregator;
import com.thorstenmarx.webtools.api.analytics.query.Query;
import com.thorstenmarx.webtools.api.datalayer.DataLayer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.script.ScriptException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Der Thread läuft die ganze Zeit über die Daten und erzeugt die User
 * Segmentierung
 *
 * @author thmarx
 */
public class SegmentationWorkerThread extends Thread {

	private static final Logger log = LoggerFactory.getLogger(SegmentationWorkerThread.class);

	public static final String CONSUMER_NAME = "segmentationWorker";

	private String workerName;

	private final AnalyticsDB db;
	private final ActionSystem actionSystem;
	private final ModuleManager moduleManager;
	private final DataLayer datalayer;

	private boolean shutdown = false;

	public SegmentationWorkerThread(int index, final AnalyticsDB db, final ActionSystem actionSystem, final ModuleManager moduleManager, final DataLayer datalayer) {
		this.db = db;
		this.actionSystem = actionSystem;
		this.moduleManager = moduleManager;
		this.datalayer = datalayer;
		setDaemon(true);
		this.workerName = CONSUMER_NAME + "_" + index;
	}

	public void shutdown() {
		shutdown = true;
	}

	@Override
	public void run() {
		while (!shutdown) {
			try {
				final Set<Segment> segments = actionSystem.getSegments();
				handleSegments(segments);
			} catch (Exception e) {
				log.error("", e);
			}
		}
	}

	private void handleSegments(final Set<Segment> segments) {
		final Map<String, Segment> segmentMap = segments.stream().filter((s) -> s.isActive()).collect(Collectors.toMap(Segment::getId,
				Function.identity()));

		final Map<String, HashSet<String>> userSegments = new ConcurrentHashMap<>();
		segments.stream().forEach((segment) -> handleSegment(segment, userSegments));

		userSegments.entrySet().forEach((e) -> {
			final HashSet<String> value = e.getValue();
			if (value.size() == 2) {
//				System.out.println("1: " + value);
			}
		});

		userSegments.forEach((user, segmentSet) -> {
			final SegmentData segmentData = new SegmentData();
//			System.out.println("1.5: " + segmentSet.size());
			segmentSet.forEach((s) -> {
				
				Segment seg = segmentMap.get(s);
				long validTo = System.currentTimeMillis() + seg.startTimeWindow().millis();
				segmentData.addSegment(s, validTo);
			});
//			System.out.println("2. " + segmentData.getSegments());
			datalayer.add(user, SegmentData.KEY, segmentData);
		});
		datalayer.add(MetaKeys.META.value(), MetaKeys.SEGMENTED_USERS.value(), new MetaSet<>(userSegments.keySet()));
	}

	public void forceSegmenteGeneration(final Segment segment) {
		handleSegments(Sets.newHashSet(segment));
	}

	private void handleSegment(final Segment segment, final Map<String, HashSet<String>> userSegments) {

		Query simpleQuery = Query.builder().start(segment.start()).end(segment.end()).build();

		Future<Optional<Void>> future;
		future = db.query(simpleQuery, new Aggregator<Optional<Void>>() {
			@Override
			public Optional<Void> call() throws Exception {
				DSLSegment dsl;
				if (segment instanceof AdvancedSegment) {
					AdvancedSegment aseg = (AdvancedSegment) segment;
					if (aseg.getDsl() == null) {
						aseg.setDsl(aseg.getContent());
					}
					dsl = new DSL(moduleManager, null).build(aseg.getDsl());
				} else {
					final JsonSegmentTranslator translator = new JsonSegmentTranslator();
					final String content = translator.translate(segment.getContent());

					dsl = new DSL(moduleManager, null).build(content);
				}
				documents.stream().forEach(dsl::handle);
				dsl.match();

				List<String> matchingUsers = new ArrayList<>();
				dsl.getAllUsers().stream().filter(dsl::matchs).forEach(matchingUsers::add);

				final String segID = segment.getId();
				matchingUsers.forEach((user) -> {
					HashSet<String> userSegmentSet;
					if (userSegments.containsKey(user)) {
						userSegmentSet = userSegments.get(user);
					} else {
						userSegmentSet = new HashSet<>();
						userSegments.put(user, userSegmentSet);
					}
					userSegmentSet.add(segID);
				});

				return Optional.empty();
			}
		}
		);
		try {

			future.get();
		} catch (InterruptedException | ExecutionException ex) {
			log.error("", ex);
		}
		if (true) {
			return;
		}
		List<String> users;
		if (segment instanceof AdvancedSegment) {
			try {
				AdvancedSegment aseg = (AdvancedSegment) segment;
				if (aseg.getDsl() == null) {
					aseg.setDsl(aseg.getContent());
				}

				AdvancedSegmentationQuery query = new AdvancedSegmentationQuery((AdvancedSegment) segment, moduleManager);
				users = db.queryAsync(query);
			} catch (ScriptException ex) {
				log.error("error create segement query " + segment, ex);

				return;
			}
		} else {
			final JsonSegmentTranslator translator = new JsonSegmentTranslator();
			final String dsl = translator.translate(segment.getContent());
			AdvancedSegment aseg = new AdvancedSegment(segment);
			aseg.setDsl(dsl);

			try {
				AdvancedSegmentationQuery query = new AdvancedSegmentationQuery(aseg, moduleManager);
				users = db.queryAsync(query);
			} catch (ScriptException ex) {
				log.error("error create segement query " + segment, ex);

				return;
			}
		}

		final String segID = segment.getId();

		users.forEach(
				(user) -> {
					HashSet<String> userSegmentSet;
					if (userSegments.containsKey(user)) {
						userSegmentSet = userSegments.get(user);
					} else {
						userSegmentSet = new HashSet<>();
						userSegments.put(user, userSegmentSet);
					}
					userSegmentSet.add(segID);
				}
		);
	}
}
