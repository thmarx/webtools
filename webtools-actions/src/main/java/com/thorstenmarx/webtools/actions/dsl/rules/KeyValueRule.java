package com.thorstenmarx.webtools.actions.dsl.rules;

/*-
 * #%L
 * webtools-actions
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.alibaba.fastjson.JSONObject;
import com.thorstenmarx.webtools.api.actions.Conditional;
import com.thorstenmarx.webtools.api.analytics.query.ShardDocument;
import com.thorstenmarx.webtools.collection.CounterMapMap;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Eine einfache Key-Value Rule die generisch verwendet werden aknn
 *
 * Ein User muss ein Feld (key) mit dem Wert (value) aufweisen.
 *
 * @author thmarx
 */
public class KeyValueRule implements Conditional {

	public static final String RULE = "KEYVALUE";

	private String key;
	private String[] values;

	private final CounterMapMap<String, String> results;

	private final Set<String> users;

	public KeyValueRule() {
		results = new CounterMapMap<>();
		users = new HashSet<>();
	}

	public String key() {
		return key;
	}

	public KeyValueRule key(final String key) {
		this.key = key;
		return this;
	}

	public String[] values() {
		return values;
	}

	public KeyValueRule values(final String[] values) {
		this.values = values;
		return this;
	}

	@Override
	public boolean matchs(final String userid) {
		return users.contains(userid);
	}

	@Override
	public void match() {
		results.entrySet().forEach((entry) -> {
			final String userid = entry.getKey();
			Map<String, Integer> testValues = entry.getValue();

			for (String value : values) {
				if (testValues.containsKey(value) && testValues.get(value) >= 1) {
					// Anzahl der nötigen Events ist erreicht
					users.add(userid);
				}
			}
		});
	}

	@Override
	public boolean valid() {
		return true;
	}

	@Override
	public void handle(final ShardDocument doc) {
		final String docValue = doc.document.getString(key);

		if (Arrays.stream(values).anyMatch(docValue::equals)) {
			final String userid = doc.document.getString("userid");
			results.add(userid, docValue, 1);
		}
	}

	@Override
	public boolean affected(final JSONObject event) {
		final String docValue = event.getString(key);

		if (Arrays.stream(values).anyMatch(docValue::equals)) {
			return true;
		}
		return false;
	}
	
	
}
