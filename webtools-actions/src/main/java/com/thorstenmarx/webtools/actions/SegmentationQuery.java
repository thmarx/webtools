package com.thorstenmarx.webtools.actions;

/*-
 * #%L
 * webtools-actions
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.thorstenmarx.webtools.api.analytics.Searchable;
import com.thorstenmarx.webtools.api.analytics.query.Query;
import com.thorstenmarx.webtools.api.analytics.query.ShardDocument;
import com.thorstenmarx.webtools.api.analytics.query.ShardedQuery;
import com.thorstenmarx.webtools.api.actions.model.Rule;
import com.thorstenmarx.webtools.api.actions.model.Segment;
import com.thorstenmarx.webtools.collection.CounterMapMap;
import com.thorstenmarx.webtools.collection.MapMap;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.RecursiveTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Bearbeitet ein Segment auf allen relevanten shards. die einzel Ergebnisse
 * werden gemergt.
 *
 * geliefert werden eine Liste von Usern, die in dem entsprechenden Segment
 * sind.
 *
 * @author marx
 */
public class SegmentationQuery implements ShardedQuery<List<String>, MapMap<String, String, Integer>, Segment> {

	private static final Logger log = LoggerFactory.getLogger(SegmentationQuery.class);
	
	final Segment query;

	public SegmentationQuery(Segment query) {
		this.query = query;
	}

	/**
	 * erzeugen der subtasks für die einzelnen shards.
	 *
	 * @param query
	 * @param target
	 * @return
	 */
	@Override
	public RecursiveTask<MapMap<String, String, Integer>> getSubTask(Segment query, Searchable target) {
		return new SegmentationTask(query, target);
	}

	/**
	 * mergen der einzelnen shards.
	 *
	 * @param subResults
	 * @return
	 */
	@Override
	public List<String> merge(List<MapMap<String, String, Integer>> subResults) {

		// die Ergebnisse werden zusammen gemergt
		CounterMapMap<String, String> mergedResults = new CounterMapMap<>();

		subResults.stream().forEach((shardMap) -> {
			shardMap.entrySet().forEach((e1) -> {
				final String userid = e1.getKey();
				e1.getValue().entrySet().forEach((e2) -> {
					final String sitePage = e2.getKey();
					final Integer value = e2.getValue();

					mergedResults.add(userid, sitePage, value);
				});
			});
		});

		List<String> matchingUsers = new ArrayList<>();

		// alle user, die ein Ruleset erfüllen, werden hier gesammelt
		matchingUsers.addAll(matchingUsers(query.rules(), mergedResults));

		return matchingUsers;
	}

	/**
	 * checks if one of the rule sets is satisfied by the collected data.
	 *
	 * PageView: Die in der CounterMapMap addierten hits werden ausgewertet und
	 * mit den RuleSets verglichen
	 *
	 * Score: Die in der CounterMapMap addierten Scores werden ausgewertet und
	 * mit den RuleSets verglichen
	 *
	 * @param ruleSet
	 * @return
	 */
	private List<String> matchingUsers(final Set<Rule> rules, CounterMapMap<String, String> result) {
		List<String> matchingUsers = new ArrayList<>();

		result.entrySet().stream().forEach((e) -> {
			final String userid = e.getKey();
			rules.stream().forEach((rule) -> {
				if (rule.match(e.getValue())) {
					matchingUsers.add(userid);
				}
			});
		});

		return matchingUsers;
	}

	@Override
	public Segment query() {
		return query;
	}

	/**
	 * Bearbeitet ein Segment auf einem Shard.
	 */
	static class SegmentationTask extends RecursiveTask<MapMap<String, String, Integer>> {

		private static final long serialVersionUID = -6413763517541683972L;

		final private Segment segment;
		transient final private Searchable shard;

		SegmentationTask(Segment segment, Searchable shard) {
			this.segment = segment;
			this.shard = shard;
		}

		@Override
		protected MapMap<String, String, Integer> compute() {
			if (shard.hasData(segment.start(), segment.end())) {
				Query query = Query.builder().start(segment.start()).end(segment.end()).build();
				segment.rules().forEach((rule) -> {
					rule.extendQuery(query);
				});

				try {
					CounterMapMap<String, String> counterMap = new CounterMapMap<>();
					List<ShardDocument> documents = shard.search(query);

					documents.stream()./*filter((d) -> d != null).*/forEach((ShardDocument doc) -> {
								segment.rules().forEach((rule) -> {
									handleRule(doc, rule, counterMap);
								});
							});
					return counterMap;
				} catch (IOException ex) {
					log.error("", ex);
				}
			}
			return new MapMap<>();
		}

		/**
		 * in der CounterMapMap werden die Vorkommen der einzelnen aktionen
		 * gezählt:
		 *
		 * PageView: userid -> site_page -> hits
		 *
		 * Event: userid -> site_event -> hits
		 *
		 * Score: userid -> scorename -> scorecount
		 *
		 * @param doc
		 * @param rule
		 * @param results
		 */
		private void handleRule(final ShardDocument doc, final Rule rule, final CounterMapMap<String, String> results) {
			rule.handle(doc, results);
		}

	}

}
