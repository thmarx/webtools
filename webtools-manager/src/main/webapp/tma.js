/*-
 * #%L
 * webtools-manager
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
var isDNT = navigator.doNotTrack == "yes" || navigator.doNotTrack == "1" || navigator.msDoNotTrack == "1" || window.doNotTrack == "1";
// TMA namespace
var TMA = TMA || {};

var dyn_functions = [];
var dyn_counter = 0;

TMA.DAY = 24 * 60 * 60 * 1000;
TMA.HOUR = 60 * 60 * 1000;
TMA.MINUTE = 60 * 1000;

// only implement if no native implementation is available
if (typeof Array.isArray === 'undefined') {
	Array.isArray = function (obj) {
		return !!obj && Array === obj.constructor;
		//return Object.prototype.toString.call(obj) === '[object Array]';
	}
}

TMA.setCookie = function (cname, cvalue, expire) {
	var d = new Date();
	d.setTime(d.getTime() + expire);
	var expires = "expires=" + d.toUTCString();
	var domain = "";
	if (TMA.cookieDomain) {
		domain = ";domain=" + TMA.cookieDomain;
	}
	document.cookie = cname + "=" + cvalue + "; " + expires + ";path=/" + domain;
};

TMA.getCookie = function (cname) {
	if (document.cookie.length > 0) {
		var c_start = document.cookie.indexOf(cname + "=");
		if (c_start !== -1) {
			c_start = c_start + cname.length + 1;
			var c_end = document.cookie.indexOf(";", c_start);
			if (c_end === -1) {
				c_end = document.cookie.length;
			}
			return unescape(document.cookie.substring(c_start, c_end));
		}
	}
	return "";
};
TMA.generateUUID = function () {
	var d = new Date().getTime();
	var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		var r = (d + Math.random() * 16) % 16 | 0;
		d = Math.floor(d / 16);
		return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
	});
	return uuid;
};
TMA.getUniqueID = function (cookiename, expire) {
	var aid = TMA.getCookie(cookiename);
	if (aid === null || aid === "") {
		aid = TMA.generateUUID();
	}
	// update cookie on every request
	TMA.setCookie(cookiename, aid, expire);
	return aid;
};

TMA.defaultParameters = function (TMA_WEBTOOLS) {
	TMA_WEBTOOLS.rid = TMA.getUniqueID("_tma_rid", 3 * TMA.MINUTE);
	TMA_WEBTOOLS.vid = TMA.getUniqueID("_tma_vid", 1 * TMA.HOUR);
	TMA_WEBTOOLS.uid = TMA.getUniqueID("_tma_uid", 365 * TMA.DAY);
	var currentDate = new Date();
	return  "&site=" + TMA_WEBTOOLS.site
			+ "&page=" + TMA_WEBTOOLS.page
			+ "&uid=" + TMA_WEBTOOLS.uid
			+ "&reqid=" + TMA_WEBTOOLS.rid
			+ "&vid=" + TMA_WEBTOOLS.vid
			+ "&offset=" + currentDate.getTimezoneOffset()
			+ "&_t=" + currentDate.getTime();
};
TMA.customParameters = function () {
	var customParameterString = "";
	var tma_custom_parameter;
	if (typeof tma_custom_parameter === 'function') {
		var customParameters = tma_custom_parameter();
		if (customParameters !== null && typeof customParameters === 'object') {
			for (var p in customParameters) {
				if (customParameters.hasOwnProperty(p)) {
					var value = customParameters[p]
					if (Array.isArray(value)) {
						for (var item in value) {
							customParameterString += "&c_" + p + '=' + value[item];
						}
					} else {
						customParameterString += "&c_" + p + '=' + customParameters[p];
					}
				}
			}
		}
	}
	return  customParameterString;
};
/**
 * Creates a delegate function
 * 
 * e.g. TMA.delegate(functionname, this, [param1, "param2"]);
 */
TMA.delegate = function (func, thisObj, args) {
	var params = args || arguments;
	var f = function () {
		return func.apply(thisObj, params);
	};

	f.target = thisObj;
	f.func = this;

	return f;
};
TMA.onload = function (func) {
	var oldonload = window.onload;
	if (typeof window.onload !== 'function') {
		window.onload = func;
	} else {
		window.onload = function () {
			oldonload();
			func();
		};
	}
};
TMA.onEvent = function (element, type, callback, bubble) { // 1
	if (document.addEventListener) { // 2
		document.addEventListener(type, function (event) { // 3
			if (event.target === element || event.target.id === element) { // 5
				callback.apply(event.target, [event]); // 6
			}
		}, bubble || false);
	} else {
		document.attachEvent('on' + type, function (event) { // 4 
			if (event.srcElement === element || event.srcElement.id === element) { // 5
				callback.apply(event.target, [event]); // 6
			}
		});
	}

};
/** @constructor */
TMA.WebTools = function (host, site, page) {
	this.site = site;
	this.page = page;
	this.host = host;
	this.uid = "";			// the userid
	this.rid = "";			// the requestid
	this.vid = "";			// the visitid
	this.pixelImage = new Image();
};
TMA.WebTools.prototype.page = function (page) {
	this.page = page;
};
TMA.WebTools.prototype.setCookieDomain = function (domain) {
	TMA.cookieDomain = domain;
};
TMA.WebTools.prototype.optOut = function () {
	TMA.setCookie('_tma_trackingcookie', "opt-out", 365 * TMA.DAY);
};
TMA.WebTools.prototype.dnt = function () {
	return isDNT || document.cookie.indexOf("_tma_trackingcookie=opt-out") !== -1;
}
TMA.WebTools.prototype.register = function () {
	if (!this.dnt()) {
		// opt-out cookie is not set
		// incude tracking pixle
		// user id
		this.uid = TMA.getUniqueID("_tma_uid", 365 * TMA.DAY);
		// visit id
		this.vid = TMA.getUniqueID("_tma_vid", 1 * TMA.HOUR);
		// new requestid for every request
		this.rid = TMA.generateUUID();
		TMA.setCookie("_tma_rid", this.rid, 3 * TMA.MINUTE);

//		var pixelImage = new Image();
//		pixelImage.src = this.host + "/pixel?event=pageview" + TMA.defaultParameters(this) + TMA.customParameters();
		this.track("pageview");
	}
};
TMA.WebTools.prototype.track = function (event) {
	if (!this.dnt()) {
		// opt-out cookie is not set
		this.pixelImage.src = this.host + "/pixel?event=" + event + TMA.defaultParameters(this) + TMA.customParameters();
	}
};

TMA.WebTools.prototype.score = function (scores) {
	if (!this.dnt()) {
		var scoreParameters = "";
		for (var key in scores) {
			scoreParameters += "&score_" + key + "=" + scores[key];
		}
		// opt-out cookie is not set
		var pixelImage = new Image();
		pixelImage.src = this.host + "/pixel?event=score" + scoreParameters + TMA.defaultParameters(this) + TMA.customParameters();
	}
};
// call init method if defined
var tma_webtools_init;
if (typeof tma_webtools_init === 'function') {
	tma_webtools_init();
}
