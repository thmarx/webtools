
package com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel;

/*-
 * #%L
 * webtools-manager
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.thorstenmarx.webtools.ContextListener;
import com.thorstenmarx.webtools.api.actions.model.Segment;
import com.thorstenmarx.webtools.api.analytics.Events;
import com.thorstenmarx.webtools.api.model.Site;
import com.thorstenmarx.webtools.manager.services.SiteService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

/**
 * @author Thorsten Marx
 */
public class NewEventRulePanel extends Panel {

	private Site selectedSite;
	private Events selectedEvent;

	
	private final DropDownChoice<Events> eventSelector;
	private final DropDownChoice<Site> siteSelector;

	public NewEventRulePanel(String id, final IModel<Segment> model) {
		super(id);
		setDefaultModel(new CompoundPropertyModel<>(this));
		
		SiteService siteService = ContextListener.INJECTOR_PROVIDER.injector().getInstance(SiteService.class);
		Collection<Site> sitesTemp = siteService.all();

		List<Site> sites = new ArrayList<>(sitesTemp);

		IModel<List<? extends Site>> siteChoices = new AbstractReadOnlyModel<List<? extends Site>>() {
			private static final long serialVersionUID = -2583290457773357445L;

			@Override
			public List<Site> getObject() {
				return sites;
			}
		};

		siteSelector = new DropDownChoice<>("site",
				new PropertyModel<>(this, "selectedSite"), siteChoices, new IChoiceRenderer<Site>() {
			private static final long serialVersionUID = 2636774494996431892L;

			@Override
			public Object getDisplayValue(Site t) {
				return t.getName();
			}

			@Override
			public String getIdValue(Site t, int i) {
				return t.getId();
			}

			@Override
			public Site getObject(String string, IModel<? extends List<? extends Site>> imodel) {
				List<? extends Site> sites = imodel.getObject();
				Optional<? extends Site> siteOptional = sites.stream().filter(site -> site.getId().equals(string)).findFirst();
				return siteOptional.orElse(null);
			}

		});
		add(siteSelector);
		
		
		IModel<List<? extends Events>> eventChoices = new AbstractReadOnlyModel<List<? extends Events>>() {
			private static final long serialVersionUID = -2583290457773357445L;

			@Override
			public List<Events> getObject() {
				return Arrays.asList(Events.values());
			}
		};

		eventSelector = new DropDownChoice<>("event",
				new PropertyModel<>(this, "selectedEvent"), eventChoices, new IChoiceRenderer<Events>() {
			private static final long serialVersionUID = 2636774494996431892L;

			@Override
			public Object getDisplayValue(Events t) {
				return t.name();
			}

			@Override
			public String getIdValue(Events t, int i) {
				return t.value();
			}

			@Override
			public Events getObject(String eventId, IModel<? extends List<? extends Events>> imodel) {
				List<? extends Events> events = imodel.getObject();
				Optional<? extends Events> siteOptional = events.stream().filter(event -> event.value().equals(eventId)).findFirst();
				return siteOptional.orElse(null);
			}

		});
		add(eventSelector);
	}
}
