package com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel;

/*-
 * #%L
 * webtools-manager
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.thorstenmarx.webtools.api.actions.model.Segment;
import java.util.Arrays;
import java.util.Locale;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

/**
 * @author Thorsten Marx
 */
public class NewLocationRulePanel extends Panel {

	
	private String countryIso;

	RepeatingView listItems;

	public NewLocationRulePanel(String id, final IModel<Segment> model) {
		super(id);

		setDefaultModel(new CompoundPropertyModel<>(this));

		

		listItems = new RepeatingView("countries");
		listItems.setOutputMarkupId(true);

		ChoiceRenderer<String> choiceRenderer = new ChoiceRenderer<String>() {
			private static final long serialVersionUID = 832467662967665874L;

			@Override
			public Object getDisplayValue(final String countryCode) {
				Locale obj = new Locale("", countryCode);
				return obj.getDisplayCountry(Locale.ENGLISH);
			}

		};
		DropDownChoice<String> countryChoice = new DropDownChoice<>("countryChoice", new PropertyModel<>(this, "countryIso"), Arrays.asList(Locale.getISOCountries()), choiceRenderer);
		countryChoice.setOutputMarkupId(true);
		
		add(countryChoice);
	}
	
	

	public String getCountryIso() {
		return countryIso;
	}

	public void setCountryIso(String countryIso) {
		this.countryIso = countryIso;
	}

	public String getCountryDisplayName(final String isocode) {
		Locale obj = new Locale("", isocode);
		return obj.getDisplayCountry(Locale.ENGLISH);
	}

}
