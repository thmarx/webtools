
package com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel;

/*-
 * #%L
 * webtools-manager
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.ContextListener;
import com.thorstenmarx.webtools.api.actions.model.Segment;
import com.thorstenmarx.webtools.api.model.Site;
import com.thorstenmarx.webtools.manager.services.SiteService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.apache.wicket.core.request.handler.IPartialPageRequestHandler;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

/**
 * @author Thorsten Marx
 */
public class CampaignRulePanel extends Panel {

	private static final long serialVersionUID = -5254848348363128626L;

	/**
	 * Construct.
	 *
	 * @param id The component id
	 */
	final IModel<Segment> model;

	private String campaign;
	private String source;
	private String medium;

	

	public CampaignRulePanel(String id, final IModel<Segment> model) {
		super(id);
		this.model = model;	

		setDefaultModel(new CompoundPropertyModel<>(this));

		add(new TextField<>("campaign", new PropertyModel<>(this, "campaign")));
		add(new TextField<>("source"));
		add(new TextField<>("medium"));
	}
	
	public void clear (final IPartialPageRequestHandler target) {
		campaign = null;
		source = null;
		medium = null;
		
		target.add(this);
	}

	public String getCampaign() {
		return campaign;
	}

	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	

}
