package com.thorstenmarx.webtools.manager.services.impl;

/*-
 * #%L
 * webtools-manager
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.api.model.Site;
import com.thorstenmarx.webtools.manager.services.SiteService;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author marx
 */
public final class FileSiteService implements SiteService {
	
	private static final Logger LOGGER = LogManager.getLogger(FileSiteService.class);

	public static final String FILENAME = "sites.xml";
	String path;

	private Sites sites = new Sites();
	private final Map<String, Site> siteMap = new ConcurrentHashMap<>();

	public FileSiteService(final String path) {
		this.path = path;
		if (!path.endsWith("/")) {
			this.path += "/";
		}

		loadSites();
	}

	private void loadSites() {
		try {
			File file = new File(path, FILENAME);
			
			if (!file.exists()) {
				return;
			}
			
			JAXBContext jaxbContext = JAXBContext.newInstance(Sites.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			this.sites = (Sites) jaxbUnmarshaller.unmarshal(file);
			
			sites.sites.stream().forEach((s) -> {
				siteMap.put(s.getId(), s);
			});
		} catch (JAXBException ex) {
			LOGGER.error("", ex);
			throw new RuntimeException(ex);
		}

	}

	private synchronized void saveSites() {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Sites.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(sites, new File(path, FILENAME));
		} catch (JAXBException ex) {
			LOGGER.error("", ex);
		}
	}

	@Override
	public void add(Site site) {
		this.siteMap.put(site.getId(), site);
		this.sites.addAll(this.siteMap.values());
		saveSites();
	}

	@Override
	public void remove(String id) {
		this.siteMap.remove(id);
		this.sites.addAll(this.siteMap.values());
		saveSites();
	}

	@Override
	public Site get(String id) {
		return siteMap.get(id);
	}

	@Override
	public Collection<Site> all() {
		return Collections.unmodifiableCollection(siteMap.values());
	}

	@XmlRootElement(name = "sites")
	@XmlAccessorType(XmlAccessType.NONE)
	public static class Sites {

		@XmlElement(name = "site")
		List<Site> sites = new ArrayList<>();

		public List<Site> getSites() {
			return sites;
		}
		public void addAll(Collection<Site> values) {
			this.sites.clear();
			this.sites.addAll(values);
		}

		public void setSites(List<Site> sites) {
			this.sites = sites;
		}
	}
}
