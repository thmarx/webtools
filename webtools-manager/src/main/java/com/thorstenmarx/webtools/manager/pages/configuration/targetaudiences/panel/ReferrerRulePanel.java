
package com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel;

/*-
 * #%L
 * webtools-manager
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.api.actions.model.Segment;
import com.thorstenmarx.webtools.tracking.referrer.Medium;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.wicket.core.request.handler.IPartialPageRequestHandler;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.TextField;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 * @author Thorsten Marx
 */
public class ReferrerRulePanel extends Panel {

	private static final long serialVersionUID = -5254848348363128626L;

	/**
	 * Construct.
	 *
	 * @param id The component id
	 */
	final IModel<Segment> model;

	private String source;
	private String medium;

	public ReferrerRulePanel(String id, final IModel<Segment> model) {
		super(id);
		this.model = model;	

		setDefaultModel(new CompoundPropertyModel<>(this));
		
//		List<String> mediumList = Arrays.stream(Medium.values()).map((m) -> m.toString()).collect(Collectors.toList());
		
		final DropDownChoice<Medium> mediumField = new DropDownChoice<>("medium", Arrays.asList(Medium.values()), new ChoiceRenderer<Medium>(){
			@Override
			public String getIdValue(Medium object, int index) {
				return object.toString();
			}

			@Override
			public Object getDisplayValue(Medium object) {
				return object.name();
			}
			
		});
		add(mediumField);
		
		final TextField sourceField = new TextField("source");
		add(sourceField);
		
	}
	
	public void clear (final IPartialPageRequestHandler target) {
		medium = null;
		source = null;
		
		target.add(this);
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	

}
