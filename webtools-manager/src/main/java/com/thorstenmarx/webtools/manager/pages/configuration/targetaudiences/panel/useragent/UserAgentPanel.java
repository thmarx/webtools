/*
 * Copyright (C) 2018 Thorsten Marx
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel.useragent;

/*-
 * #%L
 * webtools-manager
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.wicket.markup.html.panel.Panel;

/**
 *
 * @author marx
 */
abstract class UserAgentPanel extends Panel {
	
	public enum KeyChoices {
		Browser("choice.browser", "browser.group", new String[]{
			eu.bitwalker.useragentutils.Browser.CHROME.getName(),
			eu.bitwalker.useragentutils.Browser.EDGE.getName(),
			eu.bitwalker.useragentutils.Browser.FIREFOX.getName(),
			eu.bitwalker.useragentutils.Browser.IE.getName(),
			eu.bitwalker.useragentutils.Browser.LYNX.getName(),
			eu.bitwalker.useragentutils.Browser.SAFARI.getName(),}),
		Device("choice.device", "os.device", new String[]{
			eu.bitwalker.useragentutils.DeviceType.COMPUTER.getName(),
			eu.bitwalker.useragentutils.DeviceType.DMR.getName(),
			eu.bitwalker.useragentutils.DeviceType.GAME_CONSOLE.getName(),
			eu.bitwalker.useragentutils.DeviceType.MOBILE.getName(),
			eu.bitwalker.useragentutils.DeviceType.TABLET.getName(),
			eu.bitwalker.useragentutils.DeviceType.WEARABLE.getName(),
			eu.bitwalker.useragentutils.DeviceType.UNKNOWN.getName()
		}),
		OperatingSystem("choice.operatingsystem", "os.group", new String[]{
			eu.bitwalker.useragentutils.OperatingSystem.ANDROID.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.BADA.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.BLACKBERRY.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.CHROME_OS.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.GOOGLE_TV.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.IOS.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.KINDLE.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.LINUX.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.MAC_OS.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.MAC_OS_X.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.MAEMO.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.MEEGO.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.PALM.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.PSP.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.ROKU.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.SERIES40.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.SONY_ERICSSON.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.SUN_OS.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.SYMBIAN.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.UBUNTU.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.WINDOWS.getName(),
			eu.bitwalker.useragentutils.OperatingSystem.XBOX_OS.getName()
		});

		protected final String displayKey;
		protected final String key;
		protected final String[] choices;

		private KeyChoices(final String displayKey, final String key, final String[] choices) {
			this.key = key;
			this.displayKey = displayKey;
			this.choices = choices;
		}

	}
	
	public UserAgentPanel(String id) {
		super(id);
	}
	
}
