package com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences;

/*-
 * #%L
 * webtools-manager
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.google.inject.Inject;
import com.thorstenmarx.webtools.api.actions.SegmentService;
import com.thorstenmarx.webtools.api.actions.model.Segment;
import com.thorstenmarx.webtools.api.TimeWindow;
import com.thorstenmarx.webtools.manager.pages.BasePage;
import com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel.CampaignRulePanel;
import com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel.FirstVisitRulePanel;
import com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel.NewEventRulePanel;
import com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel.NewLocationRulePanel;
import com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel.NewScoreRulePanel;
import com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel.NewPageViewRulePanel;
import com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel.ReferrerRulePanel;
import com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel.useragent.BrowserRulePanel;
import com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel.useragent.OperatingSystemRulePanel;
import com.thorstenmarx.webtools.manager.pages.configuration.targetaudiences.panel.useragent.DeviceRulePanel;
import java.util.Arrays;
import org.apache.wicket.Session;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

public class AddEditSegmentPage extends BasePage {

	private static final JavaScriptResourceReference CONDTION_JS = new JavaScriptResourceReference(AddEditSegmentPage.class,
			"js/condition.js");
	private static final JavaScriptResourceReference GROUP_JS = new JavaScriptResourceReference(AddEditSegmentPage.class,
			"js/group.js");
	private static final JavaScriptResourceReference VIEWMODEL_JS = new JavaScriptResourceReference(AddEditSegmentPage.class,
			"js/viewModel.js");
	private static final CssResourceReference STYLES_CSS = new CssResourceReference(AddEditSegmentPage.class,
			"css/styles.css");
	
	@Inject
	transient private SegmentService service;

	private boolean edit = false;

	private WebMarkupContainer rulesContainer;

	TimeWindow.UNIT unit;
	long unitCount;
	
	boolean active = false;
	
	public AddEditSegmentPage() {
		super();
		setOutputMarkupId(true);
		this.edit = false;
		setDefaultModel(new Model<>(new Segment()));
		initGui();
	}

	public AddEditSegmentPage(final IModel<Segment> model) {
		super();
		this.edit = true;

		unit = model.getObject().startTimeWindow().getUnit();
		unitCount = model.getObject().startTimeWindow().getCount();
		active = model.getObject().isActive();
		setDefaultModel(model);
		initGui();
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response); //To change body of generated methods, choose Tools | Templates.
		response.render(JavaScriptHeaderItem.forUrl("https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.2/knockout-min.js"));
		
		response.render(JavaScriptHeaderItem.forReference(CONDTION_JS));
		response.render(JavaScriptHeaderItem.forReference(GROUP_JS));
		response.render(JavaScriptHeaderItem.forReference(VIEWMODEL_JS));
		response.render(CssHeaderItem.forReference(STYLES_CSS));
	}



	private void initGui() {

		Form<Segment> addSegmentForm = new Form<>("addSegmentForm",
				new CompoundPropertyModel<Segment>((IModel<Segment>) getDefaultModel()));
		add(addSegmentForm);

		Label nameLabel = new Label("nameLabel", new StringResourceModel("segmentName", this, null));
		addSegmentForm.add(nameLabel);
		addSegmentForm.add(createLabelFieldWithValidation("name", "segmentName"));
		
		TextField<String> idField = new HiddenField<>("id");
		idField.setLabel(new StringResourceModel("segmentId", this, null));
		addSegmentForm.add(idField);
		
		Label activeLabel = new Label("activeLabel", new StringResourceModel("segmentActive", this, null));
		addSegmentForm.add(activeLabel);
		CheckBox activeCheckbox = new CheckBox("segmentActive", new PropertyModel<>(this, "active"));
		addSegmentForm.add(activeCheckbox);

		ChoiceRenderer<TimeWindow.UNIT> intervalRenderer = new ChoiceRenderer<>("name");
		final DropDownChoice<TimeWindow.UNIT> timeRangeDropdown = new DropDownChoice<>("timerange-interval", new PropertyModel<>(this, "unit"), Arrays.asList(TimeWindow.UNIT.values()), intervalRenderer);
		timeRangeDropdown.setRequired(true);
		addSegmentForm.add(timeRangeDropdown);
		final TextField timerangeField = new TextField("timerange-count", new PropertyModel<>(this, "unitCount"));
		timerangeField.setRequired(true);
		addSegmentForm.add(timerangeField);

		Button submitButton = new Button("submitButton") {
			private static final long serialVersionUID = 9123164874596936371L;

			@Override
			public void onSubmit() {
				Segment segment = getSegmentFromPageModel();

				service.add(segment);
				getSession().info(new StringResourceModel("segmentUpdated", this, null).getString());

				setResponsePage(new SegmentsPage());
			}
		};
		addSegmentForm.add(submitButton);

		Button cancelButton = new Button("cancelButton") {
			@Override
			public void onSubmit() {
				setResponsePage(new SegmentsPage());
			}
		};
		cancelButton.setDefaultFormProcessing(false);
		addSegmentForm.add(cancelButton);

		TextArea<String> contentArea = new TextArea<>("content");
		contentArea.add(new JSONDSLScriptValidator(getString("contenttextarea.error")));
		addSegmentForm.add(contentArea);
		
		
		addSegmentForm.add(new NewPageViewRulePanel("pageViewRuleTemplate", (IModel<Segment>) getDefaultModel()));
		addSegmentForm.add(new FirstVisitRulePanel("firstVisitRuleTemplate", (IModel<Segment>) getDefaultModel()));
		addSegmentForm.add(new NewEventRulePanel("eventRuleTemplate", (IModel<Segment>) getDefaultModel()));
		addSegmentForm.add(new NewScoreRulePanel("scoreRuleTemplate", (IModel<Segment>) getDefaultModel()));
		addSegmentForm.add(new NewLocationRulePanel("locationRuleTemplate", (IModel<Segment>) getDefaultModel()));
		addSegmentForm.add(new BrowserRulePanel("browserRuleTemplate", (IModel<Segment>) getDefaultModel()));
		addSegmentForm.add(new OperatingSystemRulePanel("osRuleTemplate", (IModel<Segment>) getDefaultModel()));
		addSegmentForm.add(new DeviceRulePanel("deviceRuleTemplate", (IModel<Segment>) getDefaultModel()));
		addSegmentForm.add(new CampaignRulePanel("campaignRuleTemplate", (IModel<Segment>) getDefaultModel()));
		addSegmentForm.add(new ReferrerRulePanel("referrerRuleTemplate", (IModel<Segment>) getDefaultModel()));
		
		Session.get().getFeedbackMessages().clear();
		addSegmentForm.add(new FeedbackPanel("feedback"));
	}

	private RequiredTextField<String> createLabelFieldWithValidation(String id, String property) {
		RequiredTextField<String> nameField = new RequiredTextField<>(id);
		nameField.setLabel(new StringResourceModel(property, this, null));

		return nameField;
	}

	@SuppressWarnings("unchecked")
	private Segment getSegmentFromPageModel() {
		Segment segment = (Segment) getDefaultModel().getObject();
		segment.start(new TimeWindow(unit, unitCount));
		segment.setActive(active);
		return segment;
	}

	public TimeWindow.UNIT getUnit() {
		return unit;
	}

	public void setUnit(TimeWindow.UNIT unit) {
		this.unit = unit;
	}

	public long getUnitCount() {
		return unitCount;
	}

	public void setUnitCount(long unitCount) {
		this.unitCount = unitCount;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String getTitle () {
		return getString("pages.segment.edit.title");
	}
	
}
