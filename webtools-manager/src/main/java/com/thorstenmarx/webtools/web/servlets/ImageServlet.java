package com.thorstenmarx.webtools.web.servlets;

/*-
 * #%L
 * webtools-manager
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.api.analytics.AnalyticsDB;
import com.thorstenmarx.webtools.ContextListener;
import com.thorstenmarx.webtools.web.utils.EventUtil;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.AsyncContext;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author thmarx
 */
@WebServlet(name = "TrackingPixel", value = "/pixel", asyncSupported = true)
public class ImageServlet extends HttpServlet {

	private static final Logger LOGGER = LogManager.getLogger(ImageServlet.class);

	private static final Logger EVENT_LOGGER = LogManager.getLogger("EventLogger");

	private static byte[] image = null;

	private final EventUtil eventUtil;

	public ImageServlet() {
		this.eventUtil = new EventUtil();
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		try {
			ImageServlet.image = BufferedImageToByte(getImage());
		} catch (IOException e) {
			throw new ServletException(e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setHeader("Content-Type", "image/gif");
		response.setHeader("Content-Length", String.valueOf(image.length));
		response.setHeader("Content-Disposition", "inline; filename=\"1x1.gif\"");

		response.getOutputStream().write(image);

		final AsyncContext asyncContext = request.startAsync(request, response);

		asyncContext.start(() -> {
			try {
				Map<String, Map<String, Object>> event = eventUtil.getEventData((HttpServletRequest) asyncContext.getRequest());

				ContextListener.INJECTOR_PROVIDER.injector().getInstance(AnalyticsDB.class).track(event);
				
			} finally {

				asyncContext.complete();
			}
		});
	}

	private BufferedImage getImage() throws IOException {
		URL url = this.getClass().getResource("1x1.gif");
		return ImageIO.read(url);
	}

	private static byte[] BufferedImageToByte(BufferedImage bild) {
		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			ImageIO.write(bild, "gif", byteArrayOutputStream);
			byte[] imageData = byteArrayOutputStream.toByteArray();
			return imageData;
		} catch (IOException ex) {
			LOGGER.error("", ex);
		}

		return null;
	}
}
