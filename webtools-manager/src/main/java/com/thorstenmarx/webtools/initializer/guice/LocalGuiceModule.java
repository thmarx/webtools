package com.thorstenmarx.webtools.initializer.guice;

/*-
 * #%L
 * webtools-manager
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.thorstenmarx.webtools.api.analytics.AnalyticsDB;
import com.thorstenmarx.webtools.base.Configuration;
import com.thorstenmarx.webtools.ContextListener;
import com.thorstenmarx.webtools.analytics.db.DefaultAnalyticsDb;
import com.thorstenmarx.webtools.api.configuration.Registry;
import com.thorstenmarx.webtools.api.datalayer.DataLayer;
import com.thorstenmarx.webtools.api.entities.Entities;
import com.thorstenmarx.webtools.api.execution.Executor;
import com.thorstenmarx.webtools.configuration.RegistryImpl;
import com.thorstenmarx.webtools.datalayer.DefaultDataLayer;
import com.thorstenmarx.webtools.entities.EntitiesImpl;
import com.thorstenmarx.webtools.api.location.LocationProvider;
import java.io.File;
import javax.xml.bind.JAXBException;
import net.engio.mbassy.bus.MBassador;

public class LocalGuiceModule extends AbstractGuiceModule {

	public LocalGuiceModule() {
	}

	@Provides
	@Singleton
	private AnalyticsDB analyticsDB(final Configuration config, final LocationProvider locationProvider, final MBassador mbassador, final Executor executor) {
		if (ContextListener.STATE.shuttingDown()) {
			return null;
		}

		DefaultAnalyticsDb db = new DefaultAnalyticsDb(config, mbassador, executor);

		initAnalyticsFilters(db, locationProvider);

		return db;
	}

	@Provides
	@Singleton
	private Entities entities() {
		EntitiesImpl entities = new EntitiesImpl("./webtools/data/entities");
		entities.open();
		return entities;
	}

	@Provides
	@Singleton
	private Registry registry() {
		RegistryImpl registry = new RegistryImpl("./webtools/data/registry");
		registry.open();
		return registry;
	}

	@Provides
	@Singleton
	private DataLayer datalayer() throws JAXBException {
		DefaultDataLayer datalayer = new DefaultDataLayer(new File("./webtools/data"));
		datalayer.open();

		return datalayer;
	}

	@Override
	protected void configure() {
	}

}
