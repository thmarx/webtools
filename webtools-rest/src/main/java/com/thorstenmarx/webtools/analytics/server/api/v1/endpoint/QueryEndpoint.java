package com.thorstenmarx.webtools.analytics.server.api.v1.endpoint;

import com.alibaba.fastjson.JSONObject;
import com.google.inject.servlet.RequestScoped;
import com.thorstenmarx.webtools.analytics.server.api.annotations.Secured;
import com.thorstenmarx.webtools.analytics.server.api.service.Service;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.ok;

/**
 * @author Gabriel Francisco - gabfssilva@gmail.com
 */
@Produces("application/json")
@Path("/v1/query")
@RequestScoped
@Secured
public class QueryEndpoint {

	private Service service;
	
	@Inject
	public QueryEndpoint (final Service service) {
		this.service = service;
	}
	
	
	@GET
	public Response query() {
		JSONObject result = new JSONObject();
		result.put("name", service.get());
		return ok(result.toJSONString()).build();
	}

}
