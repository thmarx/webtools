package com.thorstenmarx.webtools.analytics.server;

import com.google.inject.servlet.GuiceFilter;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;
import org.glassfish.jersey.servlet.ServletContainer;

import javax.servlet.ServletException;

import static io.undertow.servlet.Servlets.listener;
import static io.undertow.servlet.Servlets.servlet;
import javax.servlet.DispatcherType;

/**
 * @author Gabriel Francisco - gabfssilva@gmail.com
 */
public class Application {
    private static Undertow server;

    public static void main(String[] args) throws ServletException {
        startContainer(9090);
    }

    public static void stopContainer(){
        server.stop();
    }

    public static void startContainer(int port) throws ServletException {
        DeploymentInfo servletBuilder = Servlets.deployment();

        servletBuilder
                .setClassLoader(Application.class.getClassLoader())
                .setContextPath("/analytics")
                .setDeploymentName("analytics.war")
                .addServlets(servlet("jerseyServlet", ServletContainer.class)
                        .setLoadOnStartup(1)
                        .addInitParam("javax.ws.rs.Application", JerseyApp.class.getName())
                        .addMapping("/api/*"))
				.addFilters(Servlets.filter("guice-filter", GuiceFilter.class).setAsyncSupported(true))
				.addFilterUrlMapping("guice-filter", "/api/*", DispatcherType.ASYNC)
				.addListeners(listener(Main.class))
				;

        DeploymentManager manager = Servlets.defaultContainer().addDeployment(servletBuilder);
        manager.deploy();
        PathHandler path = Handlers.path(Handlers.redirect("/analytics"))
                .addPrefixPath("/analytics", manager.start());

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				server.stop();
			}
		});
		
        server =
                Undertow
                        .builder()
                        .addHttpListener(port, "localhost")
                        .setHandler(path)
                        .build();

        server.start();
		
		System.out.println("server started!");
		System.out.println("to stop: ctrl-c");
    }
}