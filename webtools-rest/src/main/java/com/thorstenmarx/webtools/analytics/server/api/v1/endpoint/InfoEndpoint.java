package com.thorstenmarx.webtools.analytics.server.api.v1.endpoint;

import com.alibaba.fastjson.JSONObject;
import com.google.inject.servlet.RequestScoped;
import com.thorstenmarx.webtools.analytics.server.api.annotations.Secured;
import com.thorstenmarx.webtools.analytics.server.api.service.Service;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.ok;

/**
 * @author Gabriel Francisco - gabfssilva@gmail.com
 */
@Produces("application/json")
@Path("/v1/info")
@RequestScoped
public class InfoEndpoint {

	private Service service;
	
	@Inject
	public InfoEndpoint (final Service service) {
		this.service = service;
	}
	
	
	@GET
	public Response info() {
		JSONObject result = new JSONObject();
		result.put("state", "ready");
		return ok(result.toJSONString()).build();
	}

}
