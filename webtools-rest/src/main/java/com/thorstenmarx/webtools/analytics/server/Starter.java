package com.thorstenmarx.webtools.analytics.server;

import java.util.concurrent.Callable;
import picocli.CommandLine;

/**
 *
 * @author marx
 */
public class Starter {
	public static void main(String... args) {
		CommandLine.call(new Status(), System.err, args);
	}
	
	@CommandLine.Command(name = "status", subcommands = Name.class)
	public static class Status implements Callable<Void> {

		@CommandLine.Option(names = {"-a", "--algorithm"}, description = "MD5, SHA-1, SHA-256, ...")
		private String algorithm = "MD5";
		
		@Override
		public Void call() throws Exception {
			System.out.println("staus " + algorithm);
			return null;
		}		
	}
	
	@CommandLine.Command(name = "name")
	public static class Name implements Callable<Void> {

		@CommandLine.ParentCommand
		private Status parent;
		
		@CommandLine.Option(names = {"-n", "--name"}, description = "Hund, Katze oder Maus")
		private String name = "name";
		
		@Override
		public Void call() throws Exception {
			System.out.println("name: " + name + " parent.status=" + parent.algorithm);
			return null;
		}
		
	}
}
