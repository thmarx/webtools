/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.webtools.analytics.server;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;
import com.thorstenmarx.webtools.analytics.server.api.service.Service;
import java.io.File;
import java.io.IOException;
import javax.servlet.ServletContextEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * User: Renato
 */
public class Main extends GuiceServletContextListener {

	private static final Logger LOGGER = LogManager.getLogger(Main.class);
	
	public static Injector injector;

	@Override
	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		super.contextDestroyed(servletContextEvent); //To change body of generated methods, choose Tools | Templates.
		
	}

	
	
	@Override
	protected Injector getInjector() {
		System.out.println("Getting injector");

		injector = Guice.createInjector(new ServletModule() {
			// Configure your IOC
			@Override
			protected void configureServlets() {
				bind(Service.class);
			}
		});

		return injector;
	}
}
