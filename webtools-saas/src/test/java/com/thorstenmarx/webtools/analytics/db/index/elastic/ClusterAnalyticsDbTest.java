/**
 * WebTools-Platform
 * Copyright (C) 2016-2018  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.analytics.db.index.elastic;

/*-
 * #%L
 * webtools-analytics
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.analytics.db.*;
import com.alibaba.fastjson.JSONObject;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import static org.assertj.core.api.Assertions.*;
import org.testng.annotations.Test;
import com.thorstenmarx.webtools.api.location.Location;
import com.thorstenmarx.webtools.api.analytics.Fields;
import com.thorstenmarx.webtools.api.analytics.query.Aggregator;
import com.thorstenmarx.webtools.api.analytics.query.Query;
import com.thorstenmarx.webtools.base.Configuration;
import java.util.concurrent.TimeUnit;
import net.engio.mbassy.bus.MBassador;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pl.allegro.tech.embeddedelasticsearch.EmbeddedElastic;
import pl.allegro.tech.embeddedelasticsearch.PopularProperties;

/**
 *
 * @author thmarx
 */
public class ClusterAnalyticsDbTest /*extends AbstractElasticsearchIntegrationTest*/ {

	MockedExecutor executor = new MockedExecutor();
	EmbeddedElastic elasticServer;

	@BeforeClass
	public void setup() throws Exception {
		elasticServer = EmbeddedElastic.builder()
				.withElasticVersion("6.3.0")
				.withSetting(PopularProperties.TRANSPORT_TCP_PORT, 9460)
				.withSetting(PopularProperties.HTTP_PORT, 9461)
				.withSetting(PopularProperties.CLUSTER_NAME, "webtools_cluster")
				.withStartTimeout(1, TimeUnit.MINUTES)
				.build();
		elasticServer.start();
	}

	@AfterClass
	public void shutdown() {
		elasticServer.stop();
	}

	/**
	 * Test of open method, of class AnalyticsDb.
	 */
	@Test(enabled = true, groups = "integration")
	public void testAnalyticsDb() throws Exception {

		System.out.println("running analytics db test");

		Configuration config = Configuration.empty();
		config.put("elastic", "index", "webtools-" + System.currentTimeMillis());

		RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(
				new HttpHost("localhost", 9461, "http")
		));

		try (ClusterAnalyticsDb instance = new ClusterAnalyticsDb(config, new MBassador(), executor, client)) {
			instance.addFilter(object -> {
				if (object.containsKey("useragent")) {
					String userAgentString = object.getString("useragent");
					eu.bitwalker.useragentutils.UserAgent ua = eu.bitwalker.useragentutils.UserAgent.parseUserAgentString(userAgentString);
					if (ua != null) {
						object.put("browser.name", ua.getBrowser().getName());
						object.put("browser.group", ua.getBrowser().getGroup().getName());
						object.put("browser.version", ua.getBrowserVersion().getVersion());
						object.put("os.name", ua.getOperatingSystem().getName());
						object.put("os.group", ua.getOperatingSystem().getGroup().getName());
						object.put("os.type", ua.getOperatingSystem().getDeviceType().getName());
					}
				}
			});
			instance.open();
			long timestamp = System.currentTimeMillis();
			JSONObject data = new JSONObject();
			//		event.put(Shard.Field.TIMESTAMP.value(), ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
			data.put(Fields._TimeStamp.value(), timestamp);
			data.put("ua", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:51.0) Gecko/20100101 Firefox/51.0");
			JSONObject test = new JSONObject();
			test.put("name", "klaus");
			data.put("test", test);
			data.put("age", 25);
			data.put("average", 25.5);
			data.put("event", "pageLoaded");
			data.put("ip", "88.153.198.210");
			
			JSONObject meta = new JSONObject();
			
			Map<String, Map<String, Object>> event = new HashMap<>();
			event.put("data", data);
			event.put("meta", meta);
			instance.track(event);
			
			Thread.sleep(5000);
//		instance.index().reopen();
//		Thread.sleep(5000);
			long startTime = timestamp - (1000 * 60 * 60);
			long endTime = timestamp + (1000 * 60 * 60);
			Query query = Query.builder().start(startTime).end(endTime).term("event", "pageLoaded").build();
			Future<Map<String, Object>> future = instance.query(query, new Aggregator<Map<String, Object>>() {
				@Override
				public Map<String, Object> call() throws Exception {
					Map<String, Object> result = new HashMap<>();
					result.put("count", documents.size());
					return result;
				}
			});
			Map<String, Object> result = future.get();
			System.out.println(result);
			assertThat((int) result.get("count")).isEqualTo(1);
			query = Query.builder().start(startTime).end(endTime).term("event", "click").build();
			future = instance.query(query, new Aggregator<Map<String, Object>>() {
				@Override
				public Map<String, Object> call() throws Exception {
					Map<String, Object> result = new HashMap<>();
					result.put("count", documents.size());
					return result;
				}
			});
			result = future.get();
			assertThat((int) result.get("count")).isEqualTo(0);
			query = Query.builder().start(startTime).end(endTime).term("test.name", "klaus").tableName("default").build();
			future = instance.query(query, new Aggregator<Map<String, Object>>() {
				@Override
				public Map<String, Object> call() throws Exception {
					Map<String, Object> result = new HashMap<>();
					result.put("count", documents.size());
					return result;
				}
			});
			result = future.get();
			assertThat((int) result.get("count")).isEqualTo(1);
		}
		client.close();
	}
}
