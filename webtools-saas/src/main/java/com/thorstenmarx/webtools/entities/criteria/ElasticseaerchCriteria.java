package com.thorstenmarx.webtools.entities.criteria;

/*-
 * #%L
 * webtools-entities
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.thorstenmarx.webtools.api.entities.criteria.Restriction;
import com.thorstenmarx.webtools.api.entities.criteria.Criteria;
import com.thorstenmarx.webtools.api.entities.Serializer;
import com.thorstenmarx.webtools.entities.annotations.AnnotationHelper;
import com.thorstenmarx.webtools.api.entities.criteria.restrictions.BooleanRestriction;
import com.thorstenmarx.webtools.api.entities.criteria.restrictions.DoubleRestriction;
import com.thorstenmarx.webtools.api.entities.criteria.restrictions.FloatRestriction;
import com.thorstenmarx.webtools.api.entities.criteria.restrictions.IntegerRestriction;
import com.thorstenmarx.webtools.api.entities.criteria.restrictions.LongRestriction;
import com.thorstenmarx.webtools.api.entities.criteria.restrictions.StringRestriction;
import com.thorstenmarx.webtools.entities.store.DBEntity;
import com.thorstenmarx.webtools.entities.store.MariaDB;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author marx
 */
public class ElasticseaerchCriteria<T> implements Criteria<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElasticseaerchCriteria.class);

	final String type;
	final Class<T> typeClass;
	final MariaDB db;

	private final Set<Restriction> restrictions;

	private final AnnotationHelper<T> annotationHelper;
	
	private final Serializer<T> serializer;

	public ElasticseaerchCriteria(final String type, final Class<T> typeClass, final MariaDB db, final AnnotationHelper<T> annotationHelper, final Serializer<T> serializer) {
		this.type = type;
		this.typeClass = typeClass;
		this.db = db;
		this.annotationHelper = annotationHelper;
		this.serializer = serializer;

		restrictions = new HashSet<>();
	}

	@Override
	public Criteria add(final Restriction restriction) {
		restrictions.add(restriction);
		return this;
	}

	@Override
	public List<T> query() {
		BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
		
//		queryBuilder.must(QueryBuilders.termQuery("db_type", type));
		
		addRestrictions(queryBuilder);
		

		try {
			List<DBEntity> entities = db.query(queryBuilder);
			List<T> result = new ArrayList<>();
			for (final DBEntity entity : entities) {
				result.add(serializer.deserialize(entity.version(), entity.content()).right);
			}
			return Collections.unmodifiableList(result);
		} catch (IOException ex) {
			LOGGER.error("", ex);
		}

		return Collections.emptyList();
	}

	private void addRestrictions(final BoolQueryBuilder queryBuilder) {
		restrictions.forEach((restriction) -> {
			if (restriction instanceof StringRestriction) {
				handleStringRestriction(queryBuilder, (StringRestriction) restriction);
			} else if (restriction instanceof IntegerRestriction) {
				handleIntegerRestriction(queryBuilder, (IntegerRestriction) restriction);
			} else if (restriction instanceof LongRestriction) {
				handleLongRestriction(queryBuilder, (LongRestriction) restriction);
			} else if (restriction instanceof FloatRestriction) {
				handleFloatRestriction(queryBuilder, (FloatRestriction) restriction);
			} else if (restriction instanceof DoubleRestriction) {
				handleDoubleRestriction(queryBuilder, (DoubleRestriction) restriction);
			} else if (restriction instanceof BooleanRestriction) {
				handleBooleanRestriction(queryBuilder, (BooleanRestriction) restriction);
			}
		});
	}

	private void handleStringRestriction(final BoolQueryBuilder queryBuilder, final StringRestriction restriction) {
		queryBuilder.must(QueryBuilders.termQuery(restriction.name, restriction.value.toLowerCase()));
	}
	
	private void handleBooleanRestriction(final BoolQueryBuilder queryBuilder, final BooleanRestriction restriction) {
		queryBuilder.must(QueryBuilders.termQuery(restriction.name, String.valueOf(restriction.value)));
	}

	private void handleIntegerRestriction(final BoolQueryBuilder queryBuilder, final IntegerRestriction restriction) {
		switch (restriction.mode) {
			case EQ:
				queryBuilder.must(QueryBuilders.termQuery(restriction.name, restriction.value));
//				queryBuilder.add(IntPoint.newExactQuery(restriction.name, restriction.value), BooleanClause.Occur.MUST);
				break;
			case GT:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).gt(restriction.value));
//				queryBuilder.add(IntPoint.newRangeQuery(restriction.name, Math.addExact(restriction.value, 1), Integer.MAX_VALUE), BooleanClause.Occur.MUST);
				break;
			case GTE:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).gte(restriction.value));
//				queryBuilder.add(IntPoint.newRangeQuery(restriction.name, restriction.value, Integer.MAX_VALUE), BooleanClause.Occur.MUST);
				break;
			case LT:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).lt(restriction.value));
//				queryBuilder.add(IntPoint.newRangeQuery(restriction.name, Integer.MIN_VALUE, Math.addExact(restriction.value, -1)), BooleanClause.Occur.MUST);
				break;
			case LTE:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).lte(restriction.value));
//				queryBuilder.add(IntPoint.newRangeQuery(restriction.name, Integer.MIN_VALUE, restriction.value), BooleanClause.Occur.MUST);
				break;
		}
	}

	private void handleLongRestriction(final BoolQueryBuilder queryBuilder, final LongRestriction restriction) {
		switch (restriction.mode) {
			case EQ:
				queryBuilder.must(QueryBuilders.termQuery(restriction.name, restriction.value));
//				queryBuilder.add(LongPoint.newExactQuery(restriction.name, restriction.value), BooleanClause.Occur.MUST);
				break;
			case GT:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).gt(restriction.value));
//				queryBuilder.add(LongPoint.newRangeQuery(restriction.name, Math.addExact(restriction.value, 1), Long.MAX_VALUE), BooleanClause.Occur.MUST);
				break;
			case GTE:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).gte(restriction.value));
//				queryBuilder.add(LongPoint.newRangeQuery(restriction.name, restriction.value, Long.MAX_VALUE), BooleanClause.Occur.MUST);
				break;
			case LT:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).lt(restriction.value));
//				queryBuilder.add(LongPoint.newRangeQuery(restriction.name, Long.MIN_VALUE, Math.addExact(restriction.value, -1)), BooleanClause.Occur.MUST);
				break;
			case LTE:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).lte(restriction.value));
//				queryBuilder.add(LongPoint.newRangeQuery(restriction.name, Long.MIN_VALUE, restriction.value), BooleanClause.Occur.MUST);
				break;
		}
	}

	private void handleDoubleRestriction(final BoolQueryBuilder queryBuilder, final DoubleRestriction restriction) {
		switch (restriction.mode) {
			case EQ:
				queryBuilder.must(QueryBuilders.termQuery(restriction.name, restriction.value));
//				queryBuilder.add(DoublePoint.newExactQuery(restriction.name, restriction.value), BooleanClause.Occur.MUST);
				break;
			case GT:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).gt(restriction.value));
//				queryBuilder.add(DoublePoint.newRangeQuery(restriction.name, Double.sum(restriction.value, 1), Double.POSITIVE_INFINITY), BooleanClause.Occur.MUST);
				break;
			case GTE:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).gte(restriction.value));
//				queryBuilder.add(DoublePoint.newRangeQuery(restriction.name, restriction.value, Double.POSITIVE_INFINITY), BooleanClause.Occur.MUST);
				break;
			case LT:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).lt(restriction.value));
//				queryBuilder.add(DoublePoint.newRangeQuery(restriction.name, Double.NEGATIVE_INFINITY, Double.sum(restriction.value, 1)), BooleanClause.Occur.MUST);
				break;
			case LTE:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).lte(restriction.value));
//				queryBuilder.add(DoublePoint.newRangeQuery(restriction.name, Double.NEGATIVE_INFINITY, restriction.value), BooleanClause.Occur.MUST);
				break;
		}
	}

	private void handleFloatRestriction(final BoolQueryBuilder queryBuilder, final FloatRestriction restriction) {
		switch (restriction.mode) {
			case EQ:
				queryBuilder.must(QueryBuilders.termQuery(restriction.name, restriction.value));
//				queryBuilder.add(FloatPoint.newExactQuery(restriction.name, restriction.value), BooleanClause.Occur.MUST);
				break;
			case GT:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).gt(restriction.value));
//				queryBuilder.add(FloatPoint.newRangeQuery(restriction.name, Float.sum(restriction.value, 1f), Float.POSITIVE_INFINITY), BooleanClause.Occur.MUST);
				break;
			case GTE:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).gte(restriction.value));
//				queryBuilder.add(FloatPoint.newRangeQuery(restriction.name, restriction.value, Float.POSITIVE_INFINITY), BooleanClause.Occur.MUST);
				break;
			case LT:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).lt(restriction.value));
//				queryBuilder.add(FloatPoint.newRangeQuery(restriction.name, Float.NEGATIVE_INFINITY, (restriction.value  - 1f)), BooleanClause.Occur.MUST);
				break;
			case LTE:
				queryBuilder.must(QueryBuilders.rangeQuery(restriction.name).lte(restriction.value));
//				queryBuilder.add(FloatPoint.newRangeQuery(restriction.name, Float.NEGATIVE_INFINITY, restriction.value), BooleanClause.Occur.MUST);
				break;
		}
	}
}
