package com.thorstenmarx.webtools.entities.store;

/*-
 * #%L
 * webtools-entities
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.api.entities.Result;
import com.zaxxer.hikari.HikariDataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author marx
 */
public class MariaDB implements DB<QueryBuilder> {

	private static final Logger LOGGER = LoggerFactory.getLogger(MariaDB.class);

	final DataSource ds;
	final RestHighLevelClient elastic;
	final String type;
	private final String userKey;
	private final String indexName;

	public MariaDB(final DataSource ds, final RestHighLevelClient elastic, final String type, final String userKey) {
		this.ds = ds;
		this.elastic = elastic;
		this.type = type;
		this.userKey = userKey;
		
		this.indexName = "entities_" + userKey;
	}
	private String getIndexName () {
		return indexName;
	}

	@Override
	public void clear(final String type) {
		try (Connection connection = ds.getConnection();
				PreparedStatement st = connection.prepareStatement("DELETE FROM entities WHERE db_type = ?")) {

			st.setString(1, type);
			st.execute();

//			DeleteRequest request = new DeleteRequest("entities");
//			request.type(type);
//			elastic.delete(request);

//			SearchRequest searchRequest = new SearchRequest("entities");
//			searchRequest.types(type);
//			searchRequest.source(new SearchSourceBuilder().query(QueryBuilders.matchAllQuery()));
//			DeleteByQueryRequest request = new DeleteByQueryRequest(searchRequest);

			String jsonString = "{\n"
					+ "  \"query\": {\n"
					+ "    \"match_all\": {}\n"
					+ "  }\n"
					+ "}";

			HttpEntity entity = new NStringEntity(jsonString, ContentType.APPLICATION_JSON);
			Map<String, String> params = Collections.singletonMap("pretty", "true");
			elastic.getLowLevelClient().performRequest("POST", "/" + getIndexName() + "/" + type + "/_delete_by_query", params, entity);
			elastic.getLowLevelClient().performRequest("POST", "/" + getIndexName() + "/_refresh");


		} catch (SQLException | IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public Result<DBEntity> list(final String type, final int offset, final int limit) {
		try (Connection connection = ds.getConnection()) {

			String statement = "SELECT * FROM entities WHERE db_type = ? LIMIT ? OFFSET ?";

			int count = count(type);
			if (count == 0) {
				return Result.EMPTY;
			}
			Result<DBEntity> result = new Result<>(count, offset, limit);

			try (PreparedStatement ps = connection.prepareStatement(statement)) {
				ps.setString(1, type);
				ps.setInt(2, limit);
				ps.setInt(3, offset);
				try (ResultSet rs = ps.executeQuery()) {
					while (rs.next()) {
						DBEntity entity = createEntity(rs);
						result.add(entity);
					}
				}

			}
			return result;
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public List<DBEntity> query(final QueryBuilder queryBuilder) throws IOException {

		List<DBEntity> result = new ArrayList<>();

		SearchRequest searchRequest = new SearchRequest(getIndexName());
		searchRequest.types(type);
		searchRequest.source(new SearchSourceBuilder().query(queryBuilder));
		

		SearchResponse searchResponse = elastic.search(searchRequest);

		SearchHit[] searchHits = searchResponse.getHits().getHits();
		for (SearchHit hit : searchHits) {
			DBEntity entity = get(hit.getId());
			if (entity != null) {
				result.add(entity);
			}
		}

		return result;
	}

	@Override
	public DBEntity get(final String id) {
		try (Connection connection = ds.getConnection()) {

			String statement = "SELECT * FROM entities WHERE db_id = ?";

			try (PreparedStatement ps = connection.prepareStatement(statement)) {
				ps.setString(1, id);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {
						DBEntity entity = createEntity(rs);
						return entity;
					}
				}

			}
			return null;
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	private DBEntity createEntity(final ResultSet rs) throws SQLException {
		final String name = rs.getString("db_name");
		final String type = rs.getString("db_type");
		final String id = rs.getString("db_id");
		final String version = rs.getString("db_version");
		final String content = rs.getString("db_content");
		final DBEntity entity = new DBEntity(type, version);
		entity.id(id);
		entity.name(name);
		entity.content(content);
		return entity;
	}

	@Override
	public int count(final String type) {
		try (Connection connection = ds.getConnection()) {

			String statement = "SELECT count(db_id) as count FROM entities WHERE db_type = ?";

			try (PreparedStatement ps = connection.prepareStatement(statement)) {
				ps.setString(1, type);
				try (ResultSet rs = ps.executeQuery()) {
					if (rs.next()) {
						return rs.getInt("count");
					}
				}
			}
			return 0;
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public boolean batch(final List<DBEntity> entities) {
		try (Connection connection = ds.getConnection()) {

			String insertStatement = "INSERT INTO entities (db_id, db_name, db_type, db_content, db_version) VALUES(?, ?, ?, ?, ?)";
			String updateStatement = "UPDATE entities SET db_name = ?, db_content=?, db_version=? WHERE db_id=?";
			try (PreparedStatement insert = connection.prepareStatement(insertStatement);
					PreparedStatement update = connection.prepareStatement(updateStatement)) {

				for (final DBEntity entity : entities) {
					if (entity.isUpdate()) {
						update.setString(1, entity.name());
						update.setString(2, entity.content());
						update.setString(3, entity.version());
						update.setString(4, entity.id());

						update.execute();
					} else {
						insert.setString(1, entity.id());
						insert.setString(2, entity.name());
						insert.setString(3, entity.type());
						insert.setString(4, entity.content());
						insert.setString(5, entity.version());

						insert.execute();
					}

					IndexRequest indexRequest = new IndexRequest(getIndexName(), entity.type(), entity.id());

					final Map<String, Object> source = new HashMap<>();
					source.put("db_id", entity.id());
					source.put("db_type", entity.type());
					addAttributes(entity, source);

					indexRequest.source(source);
					elastic.index(indexRequest);
				};

				connection.commit();

				return true;
			} catch (IOException ex) {
				throw new RuntimeException(ex);
			}
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	@Override
	public boolean add(final DBEntity entity) {

		try (Connection connection = ds.getConnection()) {

			String statement;
			if (entity.isUpdate()) {
				statement = "UPDATE entities SET db_name = ?, db_content=?, db_version=? WHERE db_id=?";
			} else {
				statement = "INSERT INTO entities (db_id, db_name, db_type, db_content, db_version) VALUES(?, ?, ?, ?, ?)";
			}

			try (PreparedStatement ps = connection.prepareStatement(statement)) {
				if (entity.isUpdate()) {
					ps.setString(1, entity.name());
					ps.setString(2, entity.content());
					ps.setString(3, entity.version());
					ps.setString(4, entity.id());
				} else {
					ps.setString(1, entity.id());
					ps.setString(2, entity.name());
					ps.setString(3, entity.type());
					ps.setString(4, entity.content());
					ps.setString(5, entity.version());
				}
				ps.execute();

				IndexRequest indexRequest = new IndexRequest(getIndexName(), entity.type(), entity.id());

				final Map<String, Object> source = new HashMap<>();
				source.put("db_id", entity.id());
				source.put("db_type", entity.type());
				addAttributes(entity, source);
				
				indexRequest.setRefreshPolicy(WriteRequest.RefreshPolicy.IMMEDIATE);
				indexRequest.source(source);
				elastic.index(indexRequest);

				connection.commit();

				return true;
			} catch (IOException ex) {
				throw new RuntimeException(ex);
			}
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * Delete an entity and all attributes;
	 *
	 * @param id
	 */
	@Override
	public void delete(final String id) {
		try (Connection connection = ds.getConnection();
				PreparedStatement st = connection.prepareStatement("DELETE FROM entities WHERE db_id = ?")) {

			st.setString(1, id);
			st.execute();

			SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
			sourceBuilder.query(QueryBuilders.termQuery("db_id", id));
			SearchRequest searchRequest = new SearchRequest(getIndexName());
			searchRequest.source(sourceBuilder);
			SearchResponse searchResponse = elastic.search(searchRequest);
			searchResponse.getHits().forEach((hit) -> {
				DeleteRequest request = new DeleteRequest(getIndexName(), type, hit.getId());
				try {
					elastic.delete(request);
				} catch (IOException ex) {
					throw new RuntimeException(ex);
				}
			});

			connection.commit();
		} catch (SQLException | IOException ex) {
			throw new RuntimeException(ex);
		}
	}

	private void addAttributes(final DBEntity entity, final Map<String, Object> document) throws SQLException {
		for (DBAttribute attribute : entity.attributes().values()) {
			addAttributeToDocument(document, attribute);
		}
	}

	private void addAttributeToDocument(final Map<String, Object> document, final DBAttribute attribute) {
		if (null != attribute.type()) {
			switch (attribute.type()) {
				case BOOLEAN:
					document.put(attribute.name(), String.valueOf(attribute.value()));
					break;
				case DOUBLE:
					document.put(attribute.name(), (Double) attribute.value());
					break;
				case FLOAT:
					document.put(attribute.name(), (Float) attribute.value());
					break;
				case INTEGER:
					document.put(attribute.name(), (Integer) attribute.value());
					break;
				case LONG:
					document.put(attribute.name(), (Long) attribute.value());
					break;
				case STRING:
					document.put(attribute.name(), String.valueOf(attribute.value()));
					break;
				default:
					break;
			}
		}
	}
}
