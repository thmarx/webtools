package com.thorstenmarx.webtools.initializer.guice;

/*-
 * #%L
 * webtools-manager
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.thorstenmarx.webtools.api.analytics.AnalyticsDB;
import com.thorstenmarx.webtools.base.Configuration;
import com.thorstenmarx.webtools.ContextListener;
import com.thorstenmarx.webtools.Fields;
import com.thorstenmarx.webtools.analytics.db.ClusterAnalyticsDb;
import com.thorstenmarx.webtools.api.configuration.Registry;
import com.thorstenmarx.webtools.api.datalayer.DataLayer;
import com.thorstenmarx.webtools.api.entities.Entities;
import com.thorstenmarx.webtools.api.execution.Executor;
import com.thorstenmarx.webtools.configuration.RemoteRegistryImpl;
import com.thorstenmarx.webtools.datalayer.MariaDBDataLayer;
import com.thorstenmarx.webtools.entities.remote.RemoteEntitiesImpl;
import com.thorstenmarx.webtools.api.location.LocationProvider;
import com.zaxxer.hikari.HikariDataSource;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.sql.DataSource;
import javax.xml.bind.JAXBException;
import net.engio.mbassy.bus.MBassador;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

public class ClusterGuiceModule extends AbstractGuiceModule {

	public ClusterGuiceModule() {
	}
	
	@Provides
	@Singleton
	private DataSource datasource (final Configuration config) {
		HikariDataSource ds = new HikariDataSource();
		Configuration.Config<Map<String, Object>> mariaConfig = config.getConfig("madiadb", Map.class);
		Map<String, Object> mc = mariaConfig.get(Collections.EMPTY_MAP);
		ds.setJdbcUrl((String) mc.get(Fields.MariaDb_Url.value()));
		ds.setUsername((String) mc.get(Fields.MariaDb_Password.value()));
		ds.setPassword((String) mc.get(Fields.MariaDb_Username.value()));
		
		return ds;
	}
	@Provides
	@Singleton
	private RestHighLevelClient elastic (final Configuration config) {
		Configuration.Config<Map<String, Object>> elasticConfig = config.getConfig("elastic", Map.class);
		List<String> urls = (List<String>) elasticConfig.get(Collections.EMPTY_MAP).get("url");
		HttpHost[] hosts = (HttpHost[]) urls.stream().map(HttpHost::create).toArray();
		RestHighLevelClient elastic = new RestHighLevelClient(
				RestClient.builder(hosts)
		);
		
		return elastic;
	}


	@Provides
	@Singleton
	private AnalyticsDB analyticsDB(final Configuration config, final LocationProvider locationProvider, final MBassador mbassador, final Executor executor, final RestHighLevelClient elastic) {
		if (ContextListener.STATE.shuttingDown()) {
			return null;
		}

		ClusterAnalyticsDb db = new ClusterAnalyticsDb(config, mbassador, executor, elastic);

		initAnalyticsFilters(db, locationProvider);

		return db;
	}


	@Provides
	@Singleton
	private Entities entities(final DataSource ds, final RestHighLevelClient elastic, final Configuration config) {
		final Configuration.Config<String> userKey = config.getConfig("user", String.class);
		RemoteEntitiesImpl entities = new RemoteEntitiesImpl(ds, elastic, userKey.get("webtools"));
		entities.open();
		return entities;
	}

	@Provides
	@Singleton
	private Registry registry(final DataSource ds) {
		Registry registry = new RemoteRegistryImpl(ds);
		return registry;
	}

	@Provides
	@Singleton
	private DataLayer datalayer(final DataSource ds) throws JAXBException {
		DataLayer datalayer = new MariaDBDataLayer(ds);
		return datalayer;
	}


}
