//package com.thorstenmarx.webtools.analytics.db.index.elastic;

/*-
 * #%L
 * webtools-analytics
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
//
//import com.thorstenmarx.webtools.analytics.db.index.Index;
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.thorstenmarx.webtools.analytics.db.index.IndexDocument;
//import com.thorstenmarx.webtools.api.analytics.Fields;
//import com.thorstenmarx.webtools.api.analytics.query.Query;
//import com.thorstenmarx.webtools.api.analytics.query.ShardDocument;
//import com.thorstenmarx.webtools.base.Configuration;
//import java.io.IOException;
//import java.net.InetAddress;
//import java.net.UnknownHostException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Objects;
//import org.apache.lucene.document.Field;
//import org.apache.lucene.document.StringField;
//import org.elasticsearch.action.index.IndexResponse;
//import org.elasticsearch.action.search.SearchResponse;
//import org.elasticsearch.action.search.SearchType;
//import org.elasticsearch.client.transport.TransportClient;
//import org.elasticsearch.common.settings.Settings;
//import org.elasticsearch.common.transport.InetSocketTransportAddress;
//import org.elasticsearch.common.transport.TransportAddress;
//import org.elasticsearch.index.query.BoolQueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.elasticsearch.search.SearchHit;
//import org.elasticsearch.search.SearchHitField;
//import org.elasticsearch.transport.client.PreBuiltTransportClient;
//import org.elasticsearch.xpack.client.PreBuiltXPackTransportClient;
//
///**
// *
// * ACHTUNG: String Felder dÃ¼rfen nicht analysiert werden:
// * http://stackoverflow.com/questions/36448941/how-to-have-not-analyzed-for-all-new-indexes-by-default
// * GANZ UNTEN: https://stackoverflow.com/questions/27570365/disabling-field-analyzing-by-default-in-elastic-search
// *
// * @author marx
// */
//public class ElasticIndex implements Index {
//
//	public static final String SOURCE_FIELD = "#" + Fields.SOURCE.value();
//	public static final String TIMESTAMP_FIELD = "#" + Fields._TimeStamp.value();
//
//	final Configuration configuration;
//	private TransportClient client;
//	private String index;
//
//	public ElasticIndex(final Configuration configuration) {
//		Objects.requireNonNull(configuration);
//		this.configuration = configuration;
//	}
//
//	@Override
//	public Index open() throws UnknownHostException {
//
//		final String connectionURL = (String) configuration.get(Configuration.Field.ELASTIC_URL.value(), "localhost:9300");
//		final String username = (String) configuration.get(Configuration.Field.ELASTIC_USERNAME.value(), null);
//		final String password = (String) configuration.get(Configuration.Field.ELASTIC_PASSWORD.value(), null);
//		final String cluster = (String) configuration.get(Configuration.Field.ELASTIC_CLUSTER.value(), null);
//		index = (String) configuration.get(Configuration.Field.ELASTIC_INDEX.value(), null);
//
//		String[] urls = connectionURL.split(",");
//		List<TransportAddress> addresses = new ArrayList<>();
//		for (String url : urls) {
//			String[] parts = url.split(":");
//			addresses.add(new InetSocketTransportAddress(InetAddress.getByName(parts[0]), Integer.parseInt(parts[1])));
//		}
//
//		client = new PreBuiltXPackTransportClient(Settings.builder()
//				.put("cluster.name", cluster)
//				.put("xpack.security.user", username + ":" + password)
//				.build()
//		);
//		addresses.forEach((ta) -> {
//			client.addTransportAddress(ta);
//		});
//		
//		return this;
//	}
//
//	@Override
//	public void close() {
//		client.close();
//	}
//
//	@Override
//	public void add(IndexDocument document) {
//		final Map<String, Object> attributes = new HashMap<>();
//
//		if (!document.json.containsKey(Fields._TimeStamp.value())) {
//			document.json.put(Fields._TimeStamp.value(), System.currentTimeMillis());
//		}
//		document.json.put(SOURCE_FIELD, document.json.toJSONString());
//		flatJsonObject(null, document.json, attributes);
//
//		System.out.println(attributes);
//		IndexResponse response = client.prepareIndex(index, "event")
//				.setSource(attributes)
//				.get();
//	}
//
//	@Override
//	public List<ShardDocument> search(final Query query) {
//
//		BoolQueryBuilder boolQuery = new BoolQueryBuilder();
//		boolQuery.filter(QueryBuilders.rangeQuery((TIMESTAMP_FIELD))
//				.from(query.start())
//				.to(query.end())
//				.includeLower(true)
//				.includeUpper(true));
//
//		if (query.terms() != null && !query.terms().isEmpty()) {
//			query.terms().entrySet().forEach((e) -> {
//				boolQuery.filter(QueryBuilders.termQuery(e.getKey() + "_na", e.getValue()));
//			});
//		}
//
//		if (query.multivalueTerms() != null && !query.multivalueTerms().isEmpty()) {
//			BoolQueryBuilder multiBoolBuilder = new BoolQueryBuilder();
//			query.multivalueTerms().entrySet().forEach(entry -> {
//				for (final String value : entry.getValue()) {
//					multiBoolBuilder.should(QueryBuilders.termQuery(entry.getKey() + "_na", value));
//				}
//			});
//			boolQuery.filter(multiBoolBuilder);
//		}
//
//		System.out.println(boolQuery.toString());
//		
//		SearchResponse response = client.prepareSearch(index)
//				.setTypes("event")
//				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
//				.setQuery(boolQuery) // Query
//				.setFetchSource(true)
//				.get();
//
//		List<ShardDocument> result = new ArrayList<>();
//		for (final SearchHit hit : response.getHits().getHits()) {
//			final String sourceContent = (String) hit.getSource().get(SOURCE_FIELD);
//
//			result.add(new ShardDocument("elastic", JSONObject.parseObject(sourceContent)));
//		}
//
//		return result;
//	}
//
//	private void flatJsonObject(final String name, final JSONObject json, final Map<String, Object> doc) {
//		json.keySet().stream().forEach((key) -> {
//			// The _source field is already use in elasticsearch
//			String tempKey = key;
//			if (key.equals(Fields.SOURCE.value())) {
//				tempKey = SOURCE_FIELD;
//			} else if (key.equals(Fields._TimeStamp.value())) {
//				tempKey = TIMESTAMP_FIELD;
//			}
//			String localname = name != null ? (name + ".") : "";
//			localname += tempKey;
//			Object value = json.get(key);
//			if (value instanceof JSONArray) {
//				JSONArray array = (JSONArray) value;
//				flatJsonArray(localname, array, doc);
//			} else if (value instanceof JSONObject) {
//				flatJsonObject(localname, (JSONObject) value, doc);
//			} else {
//				handleItem(localname, value, doc);
//			}
//		});
//	}
//
//	private void flatJsonArray(final String name, final JSONArray array, final Map<String, Object> doc) {
//		array.stream().forEach((item) -> {
//			if (item instanceof JSONArray) {
//				flatJsonArray(name, (JSONArray) item, doc);
//			} else if (item instanceof JSONObject) {
//				flatJsonObject(name, (JSONObject) item, doc);
//			} else {
//				handleItem(name, item, doc);
//			}
//		});
//	}
//
//	private void handleItem(final String name, final Object value, final Map<String, Object> doc) {
//		addValue(name, value, doc);
//	}
//
//	private void addValue(String key, Object value, Map<String, Object> doc) {
//		if (value instanceof Integer) {
//			doc.put(key, (Integer) value);
//		} else if (value instanceof Long) {
//			doc.put(key, (Long) value);
//		} else if (value instanceof Float) {
//			doc.put(key, (Float) value);
//		} else if (value instanceof Double) {
//			doc.put(key, (Double) value);
//		} else if (value instanceof String) {
//			doc.put(key + "_na", (String) value);
//		} else if (value instanceof Boolean) {
//			doc.put(key, value);
//		}
//	}
//
//	@Override
//	public long size() {
//		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//	}
//}
