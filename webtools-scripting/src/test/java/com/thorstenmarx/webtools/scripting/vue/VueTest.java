package com.thorstenmarx.webtools.scripting.vue;

/*-
 * #%L
 * webtools-scripting
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.scripting.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.function.Supplier;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import org.testng.annotations.Test;

/**
 *
 * @author marx
 */
public class VueTest {

	@Test 
	public void testVue () {
		Scripting scripting = new Scripting("com/thorstenmarx/webtools/scripting/modules");
	}
	
	@Test
	public void testVue_basic() throws ScriptException, FileNotFoundException, UnsupportedEncodingException {
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
		engine.eval("var global = this;");
		System.out.println("Loading script: vue.js");
		
		
		Bindings createBindings = engine.createBindings();
		createBindings.put("console", new Object() {
//			public void log(final String message) {
//				System.out.println(message);
//			}
//			public void err(final String message) {
//				System.err.println(message);
//			}
			
		});
		
		engine.eval("load('classpath:net/arnx/nashorn/lib/promise.js')", createBindings);
		engine.eval(new InputStreamReader(new FileInputStream(new File("src/test/resources/com/thorstenmarx/webtools/scripting/vue/vue.js")), "utf-8"), createBindings);
		engine.eval(new InputStreamReader(new FileInputStream(new File("src/test/resources/com/thorstenmarx/webtools/scripting/vue/vue-router.js")), "utf-8"), createBindings);
		engine.eval(new InputStreamReader(new FileInputStream(new File("src/test/resources/com/thorstenmarx/webtools/scripting/vue/basic.js")), "utf-8"), createBindings);
		engine.eval(new InputStreamReader(new FileInputStream(new File("src/test/resources/com/thorstenmarx/webtools/scripting/vue/app.js")), "utf-8"), createBindings);

	}
}
