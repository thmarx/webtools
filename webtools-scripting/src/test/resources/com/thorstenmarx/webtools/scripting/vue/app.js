//var global = this;
//var window = this;
//var process = {env:{}};

var console = {};
console.debug = print;
console.log = print;
console.warn = print;
console.error = print;
// 
// // Step 1: Create a Vue instance
//var Vue = require('vue')
var app = new Vue({
	template: '<div>Hello World</div>'
})
//var app = new Vue()

console.log("test");

// Step 2: Create a renderer
var renderer = {};
renderer.renderToString = renderVueComponentToString;

//// Step 3: Render the Vue instance to HTML
renderer.renderToString(app, function (err, html) {
	if (err)
		throw err
	console.log(html)
	// => <div data-server-rendered="true">Hello World</div>
})

// in 2.5.0+, returns a Promise if no callback is passed:
//renderer.renderToString(app).then(function (html) {
//	console.log(html)
//}).catch(function (err) {
//	console.error(err)
//})
