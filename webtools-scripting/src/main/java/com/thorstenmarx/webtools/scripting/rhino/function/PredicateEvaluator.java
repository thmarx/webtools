package com.thorstenmarx.webtools.scripting.rhino.function;

/*-
 * #%L
 * webtools-scripting
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import org.mozilla.javascript.Context;
import org.mozilla.javascript.ScriptableObject;

public class PredicateEvaluator {

	private Context context;
	private ScriptableObject scope;

	public PredicateEvaluator(AnnotatedScriptableObject... objects) {
		context = Context.enter();
		scope = context.initStandardObjects();
		for (AnnotatedScriptableObject object : objects) {
			object.addToScope(scope);
		}
	}

	public boolean evaluate(String rule) {
		Object result = context.evaluateString(scope, rule, "<rule>", 1, null);
		return Context.toBoolean(result);
	}

	public void stop() {
		Context.exit();
	}
	
}
