package com.thorstenmarx.webtools.scripting.rhino.function;

/*-
 * #%L
 * webtools-scripting
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
public class RhinoPlay {

	public static void main(String[] args) {

		Model data = new Model()//
				.addRelative("James", "Jim", "Uncle", 50)//
				.addRelative("Linda", "Aunty Linda", "Aunt", 48)//
				.addRelative("Nicholas", "Nick", "Cousin", 32);

		PredicateEvaluator evaluator = new PredicateEvaluator(data);

		try {

			System.out.println(evaluator.evaluate("nickNameOf('Linda') == 'Aunty Linda'"));
			System.out.println(evaluator.evaluate("nickNameOf('James') != 'Johnny'"));
			
			System.out.println(evaluator.evaluate("ageOf('James') == 50"));
			System.out.println(evaluator.evaluate("ageOf('Nicholas') != 50"));
			
			System.out.println(evaluator.evaluate("myRelationTo('Nicholas') == 'Cousin'"));
			System.out.println(evaluator.evaluate("myRelationTo('Linda') != 'Nemesis'"));
			
		} finally {
			
			evaluator.stop();
		}
	}
}
