/*
 * Copyright (C) 2018 Thorsten Marx
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.scripting.rhino;

/*-
 * #%L
 * webtools-scripting
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.annotations.JSConstructor;
import org.mozilla.javascript.annotations.JSFunction;
import org.mozilla.javascript.annotations.JSGetter;

/**
 *
 * @author marx
 */
public class Promise extends ScriptableObject {

	private static final long serialVersionUID = 438270592527335642L;

	// The zero-argument constructor used by Rhino runtime to create instances
	public Promise() {
	}

	// @JSConstructor annotation defines the JavaScript constructor
	@JSConstructor
	public Promise(int a) {
		count = a;
	}

	// The class name is defined by the getClassName method
	@Override
	public String getClassName() {
		return "Promise";
	}

	// The method getCount defines the count property.
	@JSGetter
	public int getCount() {
		return count++;
	}

	// Methods can be defined the @JSFunction annotation.
	// Here we define resetCount for JavaScript.
	@JSFunction
	public void resetCount() {
		count = 0;
	}

	private int count;
}
