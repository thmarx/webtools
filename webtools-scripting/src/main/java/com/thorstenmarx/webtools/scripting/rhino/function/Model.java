package com.thorstenmarx.webtools.scripting.rhino.function;

/*-
 * #%L
 * webtools-scripting
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import java.util.HashMap;
import java.util.Map;



@SuppressWarnings("serial")
public class Model extends AnnotatedScriptableObject {

	private Map<String, String> relativesNamesToNicknames = new HashMap<>();
	private Map<String, String> relativesNamesToRelationship = new HashMap<>();
	private Map<String, Integer> relativesNamesToAge = new HashMap<>();

	@Expose
	public String nickNameOf(String friendsName) {
		return relativesNamesToNicknames.get(friendsName);
	}

	@Expose
	public String myRelationTo(String friendsName) {
		return relativesNamesToRelationship.get(friendsName);
	}

	@Expose
	public int ageOf(String friendsName) {
		if (!relativesNamesToAge.containsKey(friendsName)) {
			return 0;
		}
		return relativesNamesToAge.get(friendsName).intValue();
	}

	public Model addRelative(String name, String nickname, String relationship, int age) {
		relativesNamesToAge.put(name, Integer.valueOf(age));
		relativesNamesToNicknames.put(name, nickname);
		relativesNamesToRelationship.put(name, relationship);
		return this;
	}

}
