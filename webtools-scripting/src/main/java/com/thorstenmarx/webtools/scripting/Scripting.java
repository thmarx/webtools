package com.thorstenmarx.webtools.scripting;

/*-
 * #%L
 * webtools-scripting
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.coveo.nashorn_modules.Require;
import com.coveo.nashorn_modules.ResourceFolder;
import delight.nashornsandbox.NashornSandbox;
import delight.nashornsandbox.NashornSandboxes;
import delight.nashornsandbox.exceptions.ScriptCPUAbuseException;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Supplier;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.script.SimpleScriptContext;
import jdk.nashorn.api.scripting.NashornScriptEngine;
import net.arnx.nashorn.lib.PromiseException;

/**
 *
 * @author marx
 */
public class Scripting {

	private ScriptEngine scriptEngine;
	private final NashornSandbox sandbox;

	private final String modulePackage;
	
	public Scripting (final String modulePackage) {
		this(Collections.EMPTY_SET, modulePackage);
	}
	
	public Scripting(final Set<Class> accessableClasses, final String modulePackage) {
		this.modulePackage = modulePackage;
		sandbox = NashornSandboxes.create();
		sandbox.allowPrintFunctions(true);
		
		accessableClasses.forEach(sandbox::allow);
		sandbox.allow(Executors.class);
		sandbox.allow(CompletableFuture.class);
		sandbox.allow(PromiseException.class);
		sandbox.allow(StringWriter.class);
		sandbox.allow(StringReader.class);
		
		extractScriptEngine();
	}

	private void initContext(final ScriptContext context) throws ScriptException {
		ResourceFolder rootFolder = ResourceFolder.create(getClass().getClassLoader(), this.modulePackage, "UTF-8");
		if (context == null) {
			Require.enable((NashornScriptEngine) scriptEngine, rootFolder);
		} else {
			Require.enable((NashornScriptEngine) scriptEngine, rootFolder, context.getBindings(ScriptContext.ENGINE_SCOPE));
			this.scriptEngine.eval("load('classpath:net/arnx/nashorn/lib/promise.js')", context);
		}
	}

	public void eval(final String script) {
		eval(script, c -> {});
	}
	public void eval(final String script, final Consumer<ScriptContext> contextInitializer) {
		try {

			ScriptContext context = new SimpleScriptContext();
			context.setBindings(sandbox.createBindings(), ScriptContext.ENGINE_SCOPE);

			contextInitializer.accept(context);

			initContext(context);

			sandbox.eval(script, context);
		} catch (ScriptCPUAbuseException | ScriptException ex) {
			throw new RuntimeException(ex);
		}
	}

	private void extractScriptEngine() {
		try {
			Field f = sandbox.getClass().getDeclaredField("scriptEngine");
			f.setAccessible(true);
			scriptEngine = (ScriptEngine) f.get(sandbox); //IllegalAccessException
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
			throw new RuntimeException(ex);
		}
	}
}
