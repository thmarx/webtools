package com.thorstenmarx.webtools.cluster;

/*-
 * #%L
 * webtools-incubator
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.assertj.core.api.Assertions;
import org.jgroups.JChannel;
import org.testng.annotations.Test;

/**
 *
 * @author marx
 */
public class ClusterTest {

	@Test
	public void doTest() throws Exception {
		final Cluster cluster = new Cluster(new JChannel(), "my-cluster", "uid1");
		cluster.connect();

		cluster.setState(State.READY);
		cluster.sendState();

		cluster.disconnect();
	}

	@Test
	public void testAddAndRemoveMember() throws Exception {
		final Cluster c1 = new Cluster(new JChannel(), "test-cluster", "uid1");
		final Cluster c2 = new Cluster(new JChannel(), "test-cluster", "uid2");

		c1.connect();
		c2.connect();

		Thread.sleep(5000l);

		Assertions.assertThat(c1.getMembers().size()).isEqualTo(1);
		Assertions.assertThat(c2.getMembers().size()).isEqualTo(1);

		c2.disconnect();

		Thread.sleep(10000l);

		Assertions.assertThat(c1.getMembers().size()).isEqualTo(0);
	}

	@Test
	public void fileTransferTest() throws Exception {
		final File srcDir = new File("src/test/resources/shard_0000000001");
		final File targetDir = new File("target/test");
		delete(targetDir);
		targetDir.mkdirs();

		final Cluster c1 = new Cluster(new JChannel(), "file-cluster", "c1");
		final Cluster c2 = new Cluster(new JChannel(), "file-cluster", "c2");
		c1.connect();
		c2.connect();

		File[] files = srcDir.listFiles();
		for (final File file : files) {
			if (file.isDirectory()) {
				continue;
			}
			c1.sendFile(file, "shard_0000000001", c2.address());
		}
	}

	void delete(File f) throws IOException {
		if (f.exists() && f.isDirectory()) {
			for (File c : f.listFiles()) {
				delete(c);
			}
		}
		if (f.exists() && !f.delete()) {
			throw new FileNotFoundException("Failed to delete file: " + f);
		}
	}
}
