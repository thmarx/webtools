package com.thorstenmarx.webtools.cluster;

/*-
 * #%L
 * webtools-incubator
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import org.assertj.core.api.Assertions;
import org.jgroups.Address;
import org.jgroups.AnycastAddress;
import org.testng.annotations.Test;

/**
 *
 * @author marx
 */
public class MemberListTest {
	
	public MemberListTest() {
	}

	@Test
	public void getCoordinator() {
		final MemberList list = new MemberList();
		list.add(new Member(1l, "m1"));
		list.add(new Member(2l, "m2"));
		list.add(new Member(10l, "m3"));
		
		Assertions.assertThat(list.size()).isEqualTo(3);
		Assertions.assertThat(list.getCoordinator().getTimestamp()).isEqualTo(1l);
	}
	
}
