package com.thorstenmarx.webtools.store;

/*-
 * #%L
 * webtools-incubator
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.protonail.leveldb.jna.LevelDB;
import com.protonail.leveldb.jna.LevelDBOptions;
import com.protonail.leveldb.jna.LevelDBWriteOptions;
import java.nio.charset.Charset;

/**
 *
 * @author marx
 */
public class LevelDBTest {

	public static void main(String[] args) {
		try (LevelDBOptions options = new LevelDBOptions()) {
			options.setCreateIfMissing(true);

			try (LevelDB levelDB = new LevelDB("target/leveldb-" + System.currentTimeMillis(), options)) {
				long before = System.currentTimeMillis();
				LevelDBWriteOptions wo = new LevelDBWriteOptions();

				for (int i = 0; i < 100000; i++) {
					levelDB.put(("name " + i).getBytes(Charsets.UTF_8), ("thorsten" + i).getBytes(Charsets.UTF_8), wo);

				}

				long after = System.currentTimeMillis();

				System.out.println("took: " + (after - before) + "ms");
			}
		}
	}
}
