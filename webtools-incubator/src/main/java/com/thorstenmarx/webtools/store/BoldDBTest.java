package com.thorstenmarx.webtools.store;

/*-
 * #%L
 * webtools-incubator
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Charsets;
import com.protonail.bolt.jna.Bolt;
import com.protonail.bolt.jna.BoltBucket;
import com.protonail.bolt.jna.BoltFileMode;
import com.protonail.bolt.jna.BoltOptions;
import com.protonail.bolt.jna.BoltTransaction;
import com.protonail.leveldb.jna.LevelDBWriteOptions;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

/**
 *
 * @author marx
 */
public class BoldDBTest {

	public static void main(String[] args) {
		Bolt.init();

		try (BoltOptions options = new BoltOptions(5000)) {
			try (final Bolt bolt = new Bolt("target/boltdb-" + System.currentTimeMillis(), BoltFileMode.DEFAULT, options)) {
				long before = System.currentTimeMillis();

				final AtomicInteger counter = new AtomicInteger(0);
//				for (int i = 0; i < 100000; i++) {
//					bolt.update(new Consumer<BoltTransaction>() {
//						@Override
//						public void accept(BoltTransaction boltTransaction) {
//							try (final BoltBucket bucket = boltTransaction.createBucketIfNotExists("my-bucket".getBytes())) { // 'my-bucket' must not be exists
//								bucket.put(("name " + counter.incrementAndGet()).getBytes(Charsets.UTF_8), ("thorsten" + counter.incrementAndGet()).getBytes(Charsets.UTF_8));
//							}
//						}
//					});
//				}

				long after = System.currentTimeMillis();

				System.out.println("single Transaction took: " + (after - before) + "ms");

				before = System.currentTimeMillis();

				counter.set(0);
				bolt.update(new Consumer<BoltTransaction>() {
					@Override
					public void accept(BoltTransaction boltTransaction) {
						for (int i = 0; i < 100000; i++) {
							try (final BoltBucket bucket = boltTransaction.createBucketIfNotExists("my-bucket".getBytes())) { // 'my-bucket' must not be exists
								bucket.put(("name " + counter.incrementAndGet()).getBytes(Charsets.UTF_8), ("thorsten" + counter.incrementAndGet()).getBytes(Charsets.UTF_8));
							}
						}

					}
				});

				after = System.currentTimeMillis();

				System.out.println("multi Transaction took: " + (after - before) + "ms");
			}
		}
	}
}
