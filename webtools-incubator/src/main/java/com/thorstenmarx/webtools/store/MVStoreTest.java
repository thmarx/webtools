package com.thorstenmarx.webtools.store;

/*-
 * #%L
 * webtools-incubator
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.h2.mvstore.MVMap;
import org.h2.mvstore.MVStore;

/**
 *
 * @author marx
 */
public class MVStoreTest {
	public static void main(String[] args) {
		MVStore store = MVStore.open("target/mv_store-" + System.currentTimeMillis());
		try {
			MVMap<String, String> map = store.openMap("test");
			long before = System.currentTimeMillis();
			for (int i = 0; i < 100000; i++) {
				map.put("name " + i, "thorsten" + i);
			}
			long after = System.currentTimeMillis();
			System.out.println("took: " + (after - before) + "ms");
		} finally {
			
			store.close();
		}
	}
}
