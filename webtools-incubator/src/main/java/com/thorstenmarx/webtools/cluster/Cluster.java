package com.thorstenmarx.webtools.cluster;

/*-
 * #%L
 * webtools-incubator
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.cluster.messages.ClusterMessage;
import com.thorstenmarx.webtools.cluster.messages.FileTransportMessage;
import com.thorstenmarx.webtools.cluster.messages.StatusMessage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgroups.Address;
import org.jgroups.ChannelListener;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;

/**
 *
 * @author marx
 */
public class Cluster {

	private static final Logger LOGGER = LogManager.getLogger(Cluster.class);

	private final JChannel channel;
	private final String clustername;

	private final MemberList members;

	private final long timestamp;

	private final Timer timer;

	private State state = State.STARTING;

	private final String uid;

	public Cluster(final JChannel channel, final String clustername, final String uid) {
		this.channel = channel;
		this.clustername = clustername;
		this.members = new MemberList();
		this.timestamp = System.nanoTime();
		this.timer = new Timer();
		this.uid = uid;
	}

	public Address address() {
		return channel.getAddress();
	}

	public Cluster setState(State state) {
		this.state = state;
		return this;
	}

	public void connect() throws Exception {
		LOGGER.info("connecting to cluster " + clustername);
		channel.connect(clustername);

		channel.setReceiver(new ClusterMessageListener(this));
		channel.addChannelListener(new ClusterChannelListener(this));

		sendState();

		// send heartbeat every 5 seconds
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				try {
					sendState();
				} catch (Exception ex) {
					LOGGER.error("error sending state", ex);
				}
			}
		}, TimeUnit.SECONDS.toMillis(5), TimeUnit.SECONDS.toMillis(5));
	}

	public void disconnect() throws Exception {
		timer.cancel();
		state = State.LEAVE;
		LOGGER.info("disconnecting");
		sendState();
		channel.clearChannelListeners();
		channel.setReceiver(null);
		channel.disconnect();
	}

	public void sendState() throws Exception {
		final StatusMessage sm = new StatusMessage().setState(state).setTimestamp(timestamp);
		sm.setSource(uid);
		sendMessage(sm);
		System.out.println(sm);

//		channel.send(null, sm);
	}

	public void sendMessage(final ClusterMessage message) throws Exception {
		sendMessage(null, message);
	}

	public void sendMessage(final Address target, final ClusterMessage message) throws Exception {
		channel.send(target, message);
	}

	public void sendFile(final File source, final String shard, final Address target) throws IOException, Exception {
		try (InputStream in = new FileInputStream(source)) {
			byte[] buf = new byte[8096];
			int bytes = in.read(buf);
			if (bytes != -1) {
				FileTransportMessage message = new FileTransportMessage();
				message.setData(buf);
				message.setSize(bytes);
				message.setOffset(0);
				message.setEof(false);
				message.setSource(uid);
				message.setShard(shard);
				message.setFilename(source.getName());
				sendMessage(message);
			}

		} finally {
			FileTransportMessage message = new FileTransportMessage();
			message.setEof(true);
			message.setSource(uid);
			message.setShard(shard);
			message.setFilename(source.getName());
			sendMessage(message);
		}
	}

	public MemberList getMembers() {
		return members;
	}

	static private class ClusterChannelListener implements ChannelListener {

		private final Cluster parent;

		private ClusterChannelListener(final Cluster parent) {
			this.parent = parent;
		}

		@Override
		public void channelConnected(JChannel channel) {
			try {
				// send state to new members
				parent.sendState();
			} catch (Exception ex) {
				LOGGER.error("error sending state", ex);
			}
		}

		@Override
		public void channelDisconnected(JChannel channel) {
			// message from our one?
//			if (channel.getAddress().equals(parent.channel.address())) {
//				return;
//			}
			parent.members.removeByAddress(channel.getAddress());
		}

		@Override
		public void channelClosed(JChannel channel) {
//			parent.members.remove(channel.getAddress());
		}

	}

	static private class ClusterMessageListener extends ReceiverAdapter {

		private final Cluster parent;

		private ClusterMessageListener(final Cluster parent) {
			this.parent = parent;
		}

		@Override
		public void receive(Message message) {
			final Object messageObject = message.getObject();
			final Address address = message.getSrc();

			// message from our one?
			if (address.equals(parent.channel.address())) {
				return;
			}
			if (messageObject instanceof StatusMessage) {
				final StatusMessage sm = (StatusMessage) messageObject;

				if (State.LEAVE.equals(sm.getState())) {

					LOGGER.info("leave message received: " + sm.toString());

					parent.members.remove(sm.getSource());
				} else {
					final Member member;
					if (parent.members.has(sm.getSource())) {
						member = parent.members.get(sm.getSource());
					} else {
						member = new Member(sm.getTimestamp(), sm.getSource());
						member.setAddress(address);
					}
					member.setState(sm.getState());
					parent.members.add(member);
				}
			} else if (messageObject instanceof FileTransportMessage) {
				final FileTransportMessage tm = (FileTransportMessage) messageObject;

				final String filename = tm.getFilename();
				final String shard = tm.getShard();
				final File target = new File("target/test/" + shard);
				target.mkdirs();

				OutputStream out = files.get(tm.getFilename());

				if (out == null) {
					try {
						File tmp = new File(target, tm.getFilename());
						out = new FileOutputStream(tmp);
						files.put(tm.getFilename(), out);
					} catch (FileNotFoundException ex) {
						LOGGER.error("", ex);
					}
				}
				if (tm.isEof()) {
					try {
						files.remove(tm.getFilename()).close();
					} catch (IOException ex) {
						LOGGER.error("", ex);
					}
				} else {
					try {
						out.write(tm.getData(), tm.getOffset(), tm.getSize());
					} catch (IOException ex) {
						LOGGER.error("", ex);
					}
				}

			}
		}

		protected Map<String, OutputStream> files = new ConcurrentHashMap<>();

	}
}
