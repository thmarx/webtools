/*
 * Copyright (C) 2018 Thorsten Marx
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.cluster.leader;

/*-
 * #%L
 * webtools-incubator
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jgroups.*;
import org.jgroups.blocks.locking.LockService;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;

public class TheOne {

	private volatile AtomicBoolean becomeMaster = new AtomicBoolean(false);
	private JChannel channel;
	private LockService lock_service;
	private Thread acquiringThread;
	private Thread statusPrinter;

	public static void main(String[] args) throws Exception {
		Thread.currentThread().setName("MyMainThread");
		TheOne master = new TheOne();
		master.channel = new JChannel();
		master.channel.connect("lock-cluster");
		master.lock_service = new LockService(master.channel);
		master.startAcquiringThread();
		master.startStatusPrinterThread();
	}

	public void startAcquiringThread() {
		acquiringThread = new Thread() {
			@Override
			public void run() {
				try {
					Thread.currentThread().setName("MyAcquiringThread");
					getLock();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		acquiringThread.setDaemon(true);
		acquiringThread.start();

	}

	private void getLock() throws Exception {
		Lock lock = lock_service.getLock("mylock");
		lock.lock();
		becomeMaster.set(true);
		System.out.println(" -- I GOT THE LOCK !!!");
	}

	private void startStatusPrinterThread() {
		statusPrinter = new Thread() {
			@Override
			public void run() {
				Thread.currentThread().setName("MyStatusPrinterThread");

				while (true) {
					try {
						System.out.println("becomeMaster ? -> { " + becomeMaster + " }");
						System.out.println("thread state ? -> { " + acquiringThread.getState() + " }");
						sleep(2000L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		statusPrinter.setDaemon(true);
		statusPrinter.start();
	}
}
