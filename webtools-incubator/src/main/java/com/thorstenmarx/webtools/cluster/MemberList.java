package com.thorstenmarx.webtools.cluster;

/*-
 * #%L
 * webtools-incubator
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Ordering;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArraySet;
import org.jgroups.Address;

/**
 *
 * @author marx
 */
public class MemberList {

	public Set<Member> members = new CopyOnWriteArraySet<>();

	public int size () {
		return members.size();
	}
	
	public void add (final Member member) {
		members.add(member);
	}
	
	public boolean remove (final String uid) {
		for (final Member member : members) {
			if (member.getUid().equals(uid)) {
				return members.remove(member);
			}
		}
		return false;
	}
	public boolean removeByAddress (final Address address) {
		for (final Member member : members) {
			if (member.getAddress().equals(address)) {
				return members.remove(member);
			}
		}
		return false;
	}
	
	public boolean has(final String uid) {
		return members.stream().anyMatch((member) -> (member.getUid().equals(uid)));
	}

	public Member get(final String uid) {
		for (final Member member : members) {
			if (member.getUid().equals(uid)) {
				return member;
			}
		}

		return null;
	}

	/**
	 * Der Coordinator ist immer das älteste Mitglied im Cluster
	 * @return 
	 */
	public Member getCoordinator() {
		

		Ordering<Member> timeOrdering = Ordering.natural().onResultOf(Member::getTimestamp);

		ImmutableSortedSet<Member> sortedMembers = ImmutableSortedSet.orderedBy(
				timeOrdering).addAll(members).build();
		
		return sortedMembers.first();
	}
}
