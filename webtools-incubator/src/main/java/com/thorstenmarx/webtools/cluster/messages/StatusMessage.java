package com.thorstenmarx.webtools.cluster.messages;

/*-
 * #%L
 * webtools-incubator
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import com.thorstenmarx.webtools.cluster.State;
import java.io.Serializable;

/**
 *
 * @author marx
 */
public class StatusMessage extends ClusterMessage implements Serializable {

	@Override
	public String toString() {
		return "StatusMessage{" + "state=" + state + ", timestamp=" + timestamp + ", source=" + getSource() + '}';
	}
	
	private static final long serialVersionUID = 1L;
	
	private State state = null;
	private long timestamp;
	
	public StatusMessage () {
		
	}
	
	
	public State getState() {
		return state;
	}

	public StatusMessage setState(State state) {
		this.state = state;
		return this;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public StatusMessage setTimestamp(long timestamp) {
		this.timestamp = timestamp;
		return this;
	}
	
	
}
