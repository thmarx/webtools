/*
 * Copyright (C) 2018 Thorsten Marx
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.thorstenmarx.webtools.cluster.leader;

/*-
 * #%L
 * webtools-incubator
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.View;

public class JGroupsLeaderElectionExample {

	private static final int MAX_ROUNDS = 1_000;
	private static final int SLEEP_TIME_IN_MILLIS = 1000;

	public static void main(String[] args) throws Exception {
		JChannel channel = new JChannel();
		channel.connect("The Test Cluster");
		for (int round = 0; round < MAX_ROUNDS; round++) {
			checkLeaderStatus(channel);
			sleep();
		}

		channel.close();
	}

	private static void sleep() {
		try {
			Thread.sleep(SLEEP_TIME_IN_MILLIS);
		} catch (InterruptedException e) {
			// Ignored
		}
	}

	private static void checkLeaderStatus(JChannel channel) {
		View view = channel.getView();
		Address address = view.getMembers()
				.get(0);
		if (address.equals(channel.getAddress())) {
			System.out.println("I'm (" + channel.getAddress() + ") the leader");
		} else {
			System.out.println("I'm (" + channel.getAddress() + ") not the leader");
		}
	}
}
