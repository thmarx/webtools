package com.thorstenmarx.webtools.cluster;

/*-
 * #%L
 * webtools-incubator
 * %%
 * Copyright (C) 2016 - 2018 Thorsten Marx
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
import java.util.Objects;
import org.jgroups.Address;

/**
 *
 * @author marx
 */
public class Member {
	private final long timestamp;
	private State state = null;
	private final String uid;
	private Address address = null;
	
	public Member (final long timestamp, final String uid) {
		this.timestamp = timestamp;
		this.uid = uid;
	}

	public String getUid() {
		return uid;
	}

	
	
	public Address getAddress() {
		return address;
	}

	public Member setAddress(Address address) {
		this.address = address;
		return this;
	}

	
	
	public long getTimestamp() {
		return timestamp;
	}

	public State getState() {
		return state;
	}

	public Member setState(State state) {
		this.state = state;
		return this;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 43 * hash + Objects.hashCode(this.uid);
		hash = 43 * hash + Objects.hashCode(this.timestamp);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Member other = (Member) obj;
		if (!Objects.equals(this.uid, other.uid)) {
			return false;
		}
		if (!Objects.equals(this.timestamp, other.timestamp)) {
			return false;
		}
		return true;
	}
}
