1. MultiMaster
2. Alle Members senden ihre zu trackenden Documente an das gesamte Cluster
2.1 Jeder Member hällt einen Cache/Queue mit den noch nicht indizierten Dokumenten

3. Neues Mitgleid kommt hinzu
3.1 Ein Member mit aktuellem Stand wird gewählt
3.2 Beiden Members bekommen einen bestimmten Status
3.3 Member mit aktuellem Stand beginnt die einzelnen Shards zu übertragen
Alternative 1:
3.3.1 Shard wird deaktiviert
3.3.2 Shard wird übertragen
3.3.3 Shard wird wieder aktiviert
3.3.4 Wenn noch nicht alle Shards übertragen sind, mache weiter mit 3.3.1
Alternative 2:
3.4.1 Alle Shards werden deaktiviert, alle Daten bleiben in einem transaction log mit MemoryIndex ähnlich dem Realtime Index
3.4.2 Alle Shards werden nacheinander übertragen
3.4.3 Alle Shards werden wieder aktiviert

4. Member mit alten Daten kommt hinzu
Ganz einfach, alle Daten weren gelöscht, ab dann genau wie 3.

Idee 
====
Wir verwenden eine art blockchain
